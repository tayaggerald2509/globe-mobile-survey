webpackJsonp([13],{

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdvanceCallPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__intro_intro__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__remarks_remarks__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__data_entity_AdvanceCallEntity__ = __webpack_require__(256);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the SplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AdvanceCallPage = /** @class */ (function () {
    function AdvanceCallPage(navCtrl, navParams, formBuilder, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.introPage = __WEBPACK_IMPORTED_MODULE_2__intro_intro__["a" /* IntroPage */];
        this.remarksPage = __WEBPACK_IMPORTED_MODULE_3__remarks_remarks__["a" /* RemarksPage */];
        this.loadProgress = 10;
        this.isDisplayDateOfVisit = false;
        this.isDisplayTimeOfVisit = false;
        this.isDisplayEtaCaused = false;
        this.isDisplayAdvanceFee = false;
        this.isDisplaySMSSent = false;
        this.surveyEntity = navParams.get("surveyEntity");
        this.surveyForms = formBuilder.group({
            "dateOfVisit": ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            "timeOfVisit": ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            "EtaCaused": ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            "advanceFee": ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            "SMSSent": ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
        });
    }
    AdvanceCallPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad ");
    };
    /**
     *  TODO: recode this
     */
    AdvanceCallPage.prototype.toggleDateOfVisit = function () {
        this.isDisplayDateOfVisit = !this.isDisplayDateOfVisit;
        this.isDisplayTimeOfVisit = false;
        this.isDisplayEtaCaused = false;
        this.isDisplayAdvanceFee = false;
        this.isDisplaySMSSent = false;
    };
    AdvanceCallPage.prototype.toggleTimeOfVisit = function () {
        this.isDisplayDateOfVisit = false;
        this.isDisplayTimeOfVisit = !this.isDisplayTimeOfVisit;
        this.isDisplayEtaCaused = false;
        this.isDisplayAdvanceFee = false;
        this.isDisplaySMSSent = false;
    };
    AdvanceCallPage.prototype.toggleEtaCaused = function () {
        this.isDisplayDateOfVisit = false;
        this.isDisplayTimeOfVisit = false;
        this.isDisplayEtaCaused = !this.isDisplayEtaCaused;
        this.isDisplayAdvanceFee = false;
        this.isDisplaySMSSent = false;
    };
    AdvanceCallPage.prototype.toggleAdvanceFee = function () {
        this.isDisplayDateOfVisit = false;
        this.isDisplayTimeOfVisit = false;
        this.isDisplayEtaCaused = false;
        this.isDisplayAdvanceFee = !this.isDisplayAdvanceFee;
        this.isDisplaySMSSent = false;
    };
    AdvanceCallPage.prototype.toggleSMSSent = function () {
        this.isDisplayDateOfVisit = false;
        this.isDisplayTimeOfVisit = false;
        this.isDisplayEtaCaused = false;
        this.isDisplayAdvanceFee = false;
        this.isDisplaySMSSent = !this.isDisplaySMSSent;
    };
    AdvanceCallPage.prototype.onSubmit = function () {
        var advanceCallEntity = new __WEBPACK_IMPORTED_MODULE_5__data_entity_AdvanceCallEntity__["a" /* AdvanceCallEntity */](this.surveyForms.value["dateOfVisit"], this.surveyForms.value["timeOfVisit"], this.surveyForms.value["EtaCaused"], this.surveyForms.value["advanceFee"], this.surveyForms.value["SMSSent"], null);
        this.surveyEntity.setAdvanceCall(advanceCallEntity);
        this.navCtrl.push(this.remarksPage, {
            page: 1,
            title: "Advance call",
            surveyEntity: this.surveyEntity
        });
    };
    AdvanceCallPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-advance',template:/*ion-inline-start:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/advance-call/advance-call.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Advance call</ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content padding class="cards-bg social-cards">\n\n    <progress-bar [progress]="loadProgress"></progress-bar>\n\n    <div>\n        <br><br>\n        <label style="font-size: 16px; color:grey; margin-top: 16px;">How\'s my team: </label>\n        <label style="font-size: 16px;">Technician observation form</label>\n        <ion-label style="font-size: 24px; font-weight: bold;">Step 1:</ion-label>\n\n        <ion-label style="font-size: 18px; font-weight: bold;">Was the subscriber informed of?</ion-label>\n    </div>\n\n    <form [formGroup]="surveyForms" (ngSubmit)="onSubmit()">\n        <ion-card class="step-cards" (click)="toggleDateOfVisit();">\n\n            <ion-card-content>\n                <label no-lines no-padding style="margin: 0;">\n                    <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                    Date of visit\n                </label>\n            </ion-card-content>\n\n            <ion-list radio-group formControlName="dateOfVisit" *ngIf="isDisplayDateOfVisit">\n                <ion-row>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>Yes</ion-label>\n                            <ion-radio value="Yes" checked="true"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>No</ion-label>\n                            <ion-radio value="No" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>N/A</ion-label>\n                            <ion-radio value="NA"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n            </ion-list>\n\n        </ion-card>\n        <ion-card class="step-cards" (click)="toggleTimeOfVisit();">\n\n            <ion-card-content>\n                <div class="item item-text-wrap">\n                    <!-- <ion-list-header no-lines no-padding class="item item-text-wrap"> -->\n                    <label no-lines no-padding style="margin: 0;">\n                        <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                        Time of visit\n                    </label>\n                    <!-- </ion-list-header> -->\n                </div>\n            </ion-card-content>\n\n            <ion-list radio-group formControlName="timeOfVisit" *ngIf="isDisplayTimeOfVisit">\n                <ion-row>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>Yes</ion-label>\n                            <ion-radio value="Yes" checked="true"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>No</ion-label>\n                            <ion-radio value="No" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>N/A</ion-label>\n                            <ion-radio value="NA"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n            </ion-list>\n        </ion-card>\n\n        <ion-card class="step-cards" (click)="toggleEtaCaused();">\n\n            <ion-card-content id="first">\n                <div class="item item-text-wrap">\n                    <label no-lines no-padding style="margin: 0;">\n                        <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                        New ETA and Caused of delay (if applicable)\n                    </label>\n                </div>\n            </ion-card-content>\n\n            <ion-list radio-group formControlName="EtaCaused" *ngIf="isDisplayEtaCaused">\n                <ion-row>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>Yes</ion-label>\n                            <ion-radio value="Yes" checked="true"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>No</ion-label>\n                            <ion-radio value="No" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>N/A</ion-label>\n                            <ion-radio value="NA"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n            </ion-list>\n        </ion-card>\n\n        <ion-card class="step-cards" (click)="toggleAdvanceFee();">\n\n            <ion-card-content>\n                <div class="item item-text-wrap">\n                    <label no-lines no-padding style="margin: 0;">\n                        <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                        Advances fees to be collected\n                        (if applicable)\n                    </label>\n                </div>\n            </ion-card-content>\n\n            <ion-list radio-group formControlName="advanceFee" *ngIf="isDisplayAdvanceFee">\n                <ion-row>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>Yes</ion-label>\n                            <ion-radio value="Yes" checked="true"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>No</ion-label>\n                            <ion-radio value="No" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>N/A</ion-label>\n                            <ion-radio value="NA"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n            </ion-list>\n        </ion-card>\n\n        <ion-card class="step-cards" (click)="toggleSMSSent();">\n\n            <ion-card-content id="first">\n                <div class="item item-text-wrap">\n                    <label no-lines no-padding style="margin: 0;">\n                        <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                        Was an SMS sent if the subscriber\n                        cannot be reached\n                    </label>\n                </div>\n            </ion-card-content>\n\n            <ion-list radio-group formControlName="SMSSent" *ngIf="isDisplaySMSSent">\n                <ion-row>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>Yes</ion-label>\n                            <ion-radio value="Yes" checked="true"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>No</ion-label>\n                            <ion-radio value="No" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>N/A</ion-label>\n                            <ion-radio value="NA"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n            </ion-list>\n        </ion-card>\n\n        <ion-footer no-shadow no-border>\n            <ion-row>\n                <ion-col>\n                    <button ion-button full style="font-weight: bold;">Save</button>\n                </ion-col>\n                <ion-col>\n                    <button ion-button full type ="submit" style="font-weight: bold;">Next</button>\n                </ion-col>\n            </ion-row>\n        </ion-footer>\n    </form>\n</ion-content>\n\n\n\n<style>\n    .social-cards ion-col {\n        padding: 0;\n    }\n</style>'/*ion-inline-end:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/advance-call/advance-call.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], AdvanceCallPage);
    return AdvanceCallPage;
}());

//# sourceMappingURL=advance-call.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InstallationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__intro_intro__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__remarks_remarks__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the SplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InstallationPage = /** @class */ (function () {
    function InstallationPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.introPage = __WEBPACK_IMPORTED_MODULE_2__intro_intro__["a" /* IntroPage */];
        this.remarksPage = __WEBPACK_IMPORTED_MODULE_3__remarks_remarks__["a" /* RemarksPage */];
        this.loadProgress = 30;
    }
    InstallationPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad ");
    };
    InstallationPage.prototype.nextPage = function () {
        this.navCtrl.push(this.remarksPage, {
            page: 3,
            title: "Installation/Repair"
        });
    };
    InstallationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-installation',template:/*ion-inline-start:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/installation/installation.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Installation/Repair</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n    <progress-bar [progress]="loadProgress"></progress-bar>\n\n    <div>\n        <br><br>\n        <ion-label style="font-size: 24px; font-weight: bold;">Step 3:</ion-label>\n\n        <label style="font-size: 16px; font-weight: bold;">Onboarding subscriber on activities and estimated time to\n            complete. Perform outdoor and indoor install/repair activities.</label>\n    </div>\n\n    <div>\n        <br><br>\n        <label style="font-size: 18px; font-weight: bold;">Did the Senior technician provide an overview of</label>\n    </div>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Outdoor activity and estimate time to complete?\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isOutdoorComplete">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Indoor activity and estimate time to complete?\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isIndoorComplete">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <div>\n        <br><br>\n        <label style="font-size: 18px; font-weight: bold;">Was the install/repair team compliant with safety standards\n            during the outdoor activities?</label>\n    </div>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Wears a hard hat?\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isWearsHardHat">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Wears safety belt and strap when climbing\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isWearSafety">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Uses safety cone when working outside, in the streets\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isUsesSafety">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <div>\n        <br><br>\n        <label style="font-size: 18px; font-weight: bold;">Did the technician use the proper tools for installation /\n            repair?</label>\n    </div>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Krone puncher for jumpering\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isKronePuncher">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Laptop (not a smartphone) for speed testing\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isLaptopForTesting">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Other tools\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isOtherTools">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n    <br>\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0; font-size: 16px; font-weight: bold">\n                Was the indoor wiring properly and neatly laid out?\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isIndoorWiringProperly">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n            </ion-row>\n        </ion-list>\n    </ion-card>\n    <br>\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0; font-size: 16px; font-weight: bold">\n                Was the technician able to complete the activity with a\n                stable broadband connection delivering the subscribed speed?\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isTechCompleteActivity">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n    <br>\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0; font-size: 16px; font-weight: bold">\n                Was the install/repair team methodical in completing the\n                installation?\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isInstallComplete">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n    <br>\n    <ion-card class="step-cards">\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0; font-size: 16px; font-weight: bold">\n                Did the team tidy up the work area (pick up excess wires,\n                modem box, etc)?\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isDidTeamTidy">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n</ion-content>\n\n<ion-footer no-shadow no-border>\n    <ion-row>\n        <ion-col>\n            <button ion-button full style="font-weight: bold;">Save</button>\n        </ion-col>\n        <ion-col>\n            <button ion-button full style="font-weight: bold;" (click)="nextPage();">Next</button>\n        </ion-col>\n    </ion-row>\n</ion-footer>'/*ion-inline-end:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/installation/installation.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], InstallationPage);
    return InstallationPage;
}());

//# sourceMappingURL=installation.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WifiPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__remarks_remarks__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the WifiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var WifiPage = /** @class */ (function () {
    function WifiPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.remarksPage = __WEBPACK_IMPORTED_MODULE_2__remarks_remarks__["a" /* RemarksPage */];
        this.loadProgress = 10;
    }
    WifiPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad ");
    };
    WifiPage.prototype.nextPage = function () {
        this.navCtrl.push(this.remarksPage, {
            page: 4,
            title: "Wifi and Airties"
        });
    };
    WifiPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-wifi',template:/*ion-inline-start:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/wifi/wifi.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>WiFi and Airties</ion-title>\n    </ion-navbar>\n</ion-header>>\n\n<ion-content padding>\n\n    <progress-bar [progress]="loadProgress"></progress-bar>\n\n    <div>\n        <ion-label style="font-size: 24px; font-weight: bold;">Step 4:</ion-label>\n\n        <label style="font-size: 16px; font-weight: bold;">Testing Internet speed in all areas of the house and\n            offering Airties</label>\n    </div>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Did the technician test speed by playing WIFI 101 with Alex Diaz JSYK video for the subscriber?\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isTechDidTest">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Was the subscriber informed of the Internet speed in all areas of the house using WiFi MD?\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isSubscriberInformed">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                        <ion-item>\n                            <ion-label>N/A</ion-label>\n                            <ion-radio value="NA" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <div>\n        <br><br>\n        <label style="font-size: 18px; font-weight: bold;">Did the Senior Technician explain basic WiFi concepts using WiFi MD?</label>\n    </div>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Placement of router/ modem\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isSeniorTectDidExplainBasic">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Factors affecting WiFi signal (thick walls, distance, appliances)\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isFactorAffect">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n    <ion-card class="step-cards" (click)="toggleGreet();">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Did the Senior Technician offer Airties to maximize WiFi coverage?\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isSeniorTechOffer">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n</ion-content>\n\n<ion-footer no-shadow no-border>\n    <ion-row>\n        <ion-col>\n            <button ion-button full style="font-weight: bold;">Save</button>\n        </ion-col>\n        <ion-col>\n            <button ion-button full style="font-weight: bold;" (click)="nextPage();">Next</button>\n        </ion-col>\n    </ion-row>\n</ion-footer>'/*ion-inline-end:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/wifi/wifi.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], WifiPage);
    return WifiPage;
}());

//# sourceMappingURL=wifi.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClosingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__remarks_remarks__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ClosingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ClosingPage = /** @class */ (function () {
    function ClosingPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.remarksPage = __WEBPACK_IMPORTED_MODULE_2__remarks_remarks__["a" /* RemarksPage */];
        this.loadProgress = 80;
    }
    ClosingPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad ");
    };
    ClosingPage.prototype.nextPage = function () {
        this.navCtrl.push(this.remarksPage, {
            page: 5,
            title: "Closing"
        });
    };
    ClosingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-closing',template:/*ion-inline-start:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/closing/closing.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Closing</ion-title>\n    </ion-navbar>\n</ion-header>>\n\n<ion-content padding>\n\n    <progress-bar [progress]="loadProgress"></progress-bar>\n\n    <div>\n        <ion-label style="font-size: 24px; font-weight: bold;">Step 6:</ion-label>\n        <label style="font-size: 16px; font-weight: bold;">Thank the customer and educate about the aftersales support</label>\n    </div>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Did the technician collect the correct fees (for collect & install)? (if applicable)\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isTechCollectCorrectFees" >\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Did the technician help install and educate the customer about the Globe At Home app?\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isTechHelpCustomer">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Did the technician provide leave behind material/ WiFi MD/ magnet? (if applicable)\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isTechProvideMaterial">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Did the technician explain and had the Service Completion Form signed? (or Streamline if available) (if\n                applicable)\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isTechExplainService">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Did the technician ask, "Mayroon pa po ba akong maitutulong sa inyo?" prior to leaving?\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isTechAsk">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n</ion-content>\n\n<ion-footer no-shadow no-border>\n    <ion-row>\n        <ion-col>\n            <button ion-button full style="font-weight: bold;">Save</button>\n        </ion-col>\n        <ion-col>\n            <button ion-button full style="font-weight: bold;" (click)="nextPage();">Next</button>\n        </ion-col>\n    </ion-row>\n</ion-footer>'/*ion-inline-end:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/closing/closing.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], ClosingPage);
    return ClosingPage;
}());

//# sourceMappingURL=closing.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OverallPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__remarks_remarks__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



// import { AdvanceCallPage } from '../steps/advance-call/advance-call';
/**
 * Generated class for the SurveyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OverallPage = /** @class */ (function () {
    function OverallPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.remarksPage = __WEBPACK_IMPORTED_MODULE_2__remarks_remarks__["a" /* RemarksPage */];
        this.loadProgress = 10;
        this.isDisplayGreet = false;
        this.isDisplayPresentID = false;
        this.isDisplaySayName = false;
    }
    OverallPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad ");
    };
    OverallPage.prototype.nextPage = function () {
        this.navCtrl.push(this.remarksPage, {
            page: 6,
            title: "Overall"
        });
    };
    OverallPage.prototype.previousPage = function () {
    };
    /**
     *  TODO: recode
     */
    OverallPage.prototype.toggleGreet = function () {
        this.isDisplayGreet = !this.isDisplayGreet;
        this.isDisplayPresentID = false;
        this.isDisplaySayName = false;
    };
    OverallPage.prototype.togglePresentID = function () {
        this.isDisplayGreet = false;
        this.isDisplayPresentID = !this.isDisplayPresentID;
        this.isDisplaySayName = false;
    };
    OverallPage.prototype.toggleSayName = function () {
        this.isDisplayGreet = false;
        this.isDisplayPresentID = false;
        this.isDisplaySayName = !this.isDisplaySayName;
    };
    OverallPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-overall',template:/*ion-inline-start:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/overall/overall.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Overall</ion-title>\n    </ion-navbar>\n</ion-header>>\n\n<ion-content padding>\n\n    <progress-bar [progress]="loadProgress"></progress-bar>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0; font-size: 16px; font-weight: bold;">\n                Did the technician make use of the Customer Orientation Form (paper checklist)?\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isTechCustomerOrient">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <div>\n        <br><br>\n        <label style="font-size: 18px; font-weight: bold;">Did the install team appear pleasant and trustworthy?</label>\n    </div>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Senior\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isSenior">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Junior\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isJunior">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Did they confidently engage with the subscriber for all the 6 Step by Step Process?\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isDidStepByStep">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <div>\n        <br><br>\n        <label style="font-size: 18px; font-weight: bold;">Install/Repair team was professional because: </label>\n    </div>\n\n    <ion-card class="step-cards" (click)="toggleGreet();">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                They are wearing prescribed uniforms and shoes: Clean and not foul smelling\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isTheyWearingUniforms">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <ion-card class="step-cards">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                They are well-groomed (well-kept hair, no beard or mustache, trimmed nails, no visible tattoo or\n                piercing)\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isTheyWllGroomed">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n    <ion-card class="step-cards" (click)="toggleGreet();">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                They used disposable slipper or shoe cover when entering the subscriber\'s premise\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isTheyUsedSlipper">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <ion-card class="step-cards" (click)="toggleGreet();">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Was the service vehicle clean with materials organized inside?\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isServiceVehicleClean">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n    <br>\n    <ion-card class="step-cards" (click)="toggleGreet();">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0; font-size: 16px; font-weight: bold;">\n                Do the technicians manifest that they are indeed MAASAHAN because they know what to do and are willing\n                to serve what is expected by the subscriber?\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isTectManifestMaasahan">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n</ion-content>\n\n<ion-footer no-shadow no-border>\n    <ion-row>\n        <ion-col>\n            <button ion-button full style="font-weight: bold;">Save</button>\n        </ion-col>\n        <ion-col>\n            <button ion-button full style="font-weight: bold;" (click)="nextPage();">Next</button>\n        </ion-col>\n    </ion-row>\n</ion-footer>'/*ion-inline-end:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/overall/overall.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], OverallPage);
    return OverallPage;
}());

//# sourceMappingURL=overall.js.map

/***/ }),

/***/ 110:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CfsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__remarks_remarks__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the CfsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CfsPage = /** @class */ (function () {
    function CfsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.remarksPage = __WEBPACK_IMPORTED_MODULE_2__remarks_remarks__["a" /* RemarksPage */];
        this.loadProgress = 10;
        this.isDisplayGreet = false;
        this.isDisplayPresentID = false;
        this.isDisplaySayName = false;
    }
    CfsPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad ");
    };
    CfsPage.prototype.nextPage = function () {
        this.navCtrl.push(this.remarksPage, {
            page: 7,
            title: "CFS Technician Attributes"
        });
    };
    CfsPage.prototype.previousPage = function () {
    };
    CfsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-cfs',template:/*ion-inline-start:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/cfs/cfs.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>CFS Technician Attributes</ion-title>\n    </ion-navbar>\n</ion-header>>\n\n<ion-content padding>\n\n    <br>\n    <label style="font-size: 18px; font-weight: bold;">Rate 1-5 with 5 being the highest if teams manifested the\n        values.</label>\n\n    <ion-item no-padding style="border-bottom: 0px;">\n        <ion-label>Competency</ion-label>\n        <ion-range min="1" max="5" [(ngModel)]="compentency">\n            <ion-label range-left>1</ion-label>\n            <ion-label range-right>5</ion-label>\n        </ion-range>\n    </ion-item>\n\n    <ion-item no-padding >\n        <ion-label>Integrity</ion-label>\n        <ion-range min="1" max="5" [(ngModel)]="integrity">\n            <ion-label range-left>1</ion-label>\n            <ion-label range-right>5</ion-label>\n        </ion-range>\n    </ion-item>\n\n    <ion-item no-padding >\n        <ion-label>Malasakit</ion-label>\n        <ion-range min="1" max="5" [(ngModel)]="malasakit">\n            <ion-label range-left>1</ion-label>\n            <ion-label range-right>5</ion-label>\n        </ion-range>\n    </ion-item>\n\n    <ion-item no-padding >\n        <ion-label>Service Orientedness</ion-label>\n        <ion-range min="1" max="5" [(ngModel)]="oritendness">\n            <ion-label range-left>1</ion-label>\n            <ion-label range-right>5</ion-label>\n        </ion-range>\n    </ion-item>\n\n    <ion-item no-padding >\n        <ion-label>Eager to Learn</ion-label>\n        <ion-range min="1" max="5" [(ngModel)]="eager">\n            <ion-label range-left>1</ion-label>\n            <ion-label range-right>5</ion-label>\n        </ion-range>\n    </ion-item>\n\n</ion-content>\n\n<ion-footer no-shadow no-border>\n    <ion-row>\n        <ion-col>\n            <button ion-button full style="font-weight: bold;">Save</button>\n        </ion-col>\n        <ion-col>\n            <button ion-button full style="font-weight: bold;" (click)="nextPage();">Next</button>\n        </ion-col>\n    </ion-row>\n</ion-footer>'/*ion-inline-end:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/cfs/cfs.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], CfsPage);
    return CfsPage;
}());

//# sourceMappingURL=cfs.js.map

/***/ }),

/***/ 111:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SuccessPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_animations__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__survey_form_survey_form__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the SplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SuccessPage = /** @class */ (function () {
    function SuccessPage(navCtrl, navParams, formBuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.loginPage = __WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */];
        this.surveyPage = __WEBPACK_IMPORTED_MODULE_3__survey_form_survey_form__["a" /* SurveyPage */];
        this.splash = true;
        this.visibility = 'hidden';
        this.formGroup = formBuilder.group({
            name: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required]
        });
    }
    SuccessPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log("ionViewDidLoad ");
        setTimeout(function () {
            _this.splash = false;
            console.log(_this.splash);
        }, 4000);
    };
    SuccessPage.prototype.onSubmit = function () {
        if (this.formGroup.valid) {
            this.navCtrl.push(this.surveyPage);
        }
    };
    SuccessPage.prototype.onDone = function () {
        this.navCtrl.push(this.surveyPage);
    };
    SuccessPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-success',template:/*ion-inline-start:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/success/success.html"*/'<ion-content padding class="success-content">\n    <br>\n    <ion-row justify-content-center align-items-center padding>\n        <label style="font-size: 24px; font-weight:bold; color: white; text-align: center;">Thank you for your feedback!</label>\n    </ion-row>\n\n    <ion-row justify-content-center align-items-center>\n        <ion-img style="background-color: transparent !important;" src="../../assets/imgs/globe_character.svg" width="200px"></ion-img>\n    </ion-row>\n\n    <ion-row padding justify-content-center align-items-center>\n        <label style="font-size: 18px; color: white; font-weight:bold;">We appreciate your time and we honestly value your options.</label>\n    </ion-row>\n    <ion-row padding justify-content-center align-items-center>\n        <label style="font-size: 18px; color: white; font-weight:bold;">Our goal is to make our clients happy, so we\'ll use the information you provided to that.</label>\n    </ion-row>\n\n</ion-content>\n\n<ion-footer no-shadow no-border>\n    <button ion-button full type="submit" style="font-weight: bold;" (click)="onDone()">DONE</button>\n</ion-footer>'/*ion-inline-end:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/success/success.html"*/,
            animations: [
                Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["j" /* trigger */])('visibilityChanged', [
                    Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["g" /* state */])('shown', Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["h" /* style */])({ opacity: 1 })),
                    Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["g" /* state */])('hidden', Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["h" /* style */])({ opacity: 0 })),
                    Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["i" /* transition */])('* => *', Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["e" /* animate */])('500ms'))
                ])
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormBuilder */]])
    ], SuccessPage);
    return SuccessPage;
}());

//# sourceMappingURL=success.js.map

/***/ }),

/***/ 121:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 121;

/***/ }),

/***/ 162:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"pages/login/login.module": [
		290,
		12
	],
	"pages/splash/splash.module": [
		291,
		0
	],
	"pages/steps/advance-call/advance-call.module": [
		292,
		11
	],
	"pages/steps/cfs/cfs.module": [
		293,
		10
	],
	"pages/steps/closing/closing.module": [
		294,
		9
	],
	"pages/steps/content/content.module": [
		295,
		8
	],
	"pages/steps/installation/installation.module": [
		296,
		7
	],
	"pages/steps/intro/intro.module": [
		297,
		6
	],
	"pages/steps/overall/overall.module": [
		298,
		5
	],
	"pages/steps/remarks/remarks.module": [
		299,
		4
	],
	"pages/steps/wifi/wifi.module": [
		300,
		3
	],
	"pages/success/success.module": [
		301,
		2
	],
	"pages/survey_form/survey_form.module": [
		302,
		1
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 162;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 19:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RemarksPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__intro_intro__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__survey_form_survey_form__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__installation_installation__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__wifi_wifi__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__closing_closing__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__overall_overall__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__cfs_cfs__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__success_success__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__data_entity_RemarksEntity__ = __webpack_require__(255);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












/**
 * Generated class for the RemarksPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RemarksPage = /** @class */ (function () {
    function RemarksPage(navCtrl, navParams, formBuilder, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.introPage = __WEBPACK_IMPORTED_MODULE_2__intro_intro__["a" /* IntroPage */];
        this.SurveyPage = __WEBPACK_IMPORTED_MODULE_3__survey_form_survey_form__["a" /* SurveyPage */];
        this.installationPage = __WEBPACK_IMPORTED_MODULE_4__installation_installation__["a" /* InstallationPage */];
        this.wifiPage = __WEBPACK_IMPORTED_MODULE_5__wifi_wifi__["a" /* WifiPage */];
        this.closingPage = __WEBPACK_IMPORTED_MODULE_6__closing_closing__["a" /* ClosingPage */];
        this.overallPage = __WEBPACK_IMPORTED_MODULE_7__overall_overall__["a" /* OverallPage */];
        this.cfsPage = __WEBPACK_IMPORTED_MODULE_8__cfs_cfs__["a" /* CfsPage */];
        this.successPage = __WEBPACK_IMPORTED_MODULE_9__success_success__["a" /* SuccessPage */];
        this.loadProgress = 10;
        this.page = navParams.get("page");
        this.title = navParams.get("title");
        this.surveryEntity = navParams.get("surveryEntity");
        if (this.page == 1) {
            this.advanceCallEntity = navParams.get("advanceCallEntity");
        }
        this.surveyForms = formBuilder.group({
            "whatWentWell": ["", __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required],
            "pointsForImprove": ["", __WEBPACK_IMPORTED_MODULE_10__angular_forms__["f" /* Validators */].required]
        });
    }
    RemarksPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad ");
    };
    RemarksPage.prototype.onSubmit = function () {
        var remarksEntity = new __WEBPACK_IMPORTED_MODULE_11__data_entity_RemarksEntity__["a" /* RemarksEntity */](this.surveyForms.value["whatWentWell"], this.surveyForms.value["pointsForImprove"]);
        switch (this.page) {
            case 1:
                // this.advanceCallEntity.setRemarks(remarksEntity);
                // this.surveryEntity.setAdvanceCall(this.advanceCallEntity);
                console.log(this.advanceCallEntity);
                // this.navCtrl.push(this.introPage, {
                //     "surveyEntity" : this.surveryEntity
                // });
                break;
            case 2:
                this.navCtrl.push(this.installationPage);
                break;
            case 3:
                this.navCtrl.push(this.wifiPage);
                break;
            case 4:
                this.navCtrl.push(this.closingPage);
                break;
            case 5:
                this.navCtrl.push(this.overallPage);
                break;
            case 6:
                this.navCtrl.push(this.cfsPage);
                break;
            default:
                this.navCtrl.push(this.successPage);
                break;
        }
    };
    RemarksPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-remarks',template:/*ion-inline-start:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/remarks/remarks.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>{{title}}</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content padding>\n\n    <div>\n        <br><br>\n        <label style="font-size: 24px; font-weight: bold;">Remarks</label><br>\n        <label style="font-size: 16px;">Indicate outstanding or critical points both positive and for improvement.</label>\n    </div>\n\n    <form [formGroup]="surveyForms" (ngSubmit)="onSubmit()">\n\n        <ion-item no-padding>\n            <ion-label floating>What went well</ion-label>\n            <ion-textarea class="formarea" formControlName="whatWentWell" rows="5"></ion-textarea>\n        </ion-item>\n        <ion-item no-padding>\n            <ion-label floating>Points for improvement</ion-label>\n            <ion-textarea class="formarea"  formControlName="pointsForImprove" row="5"></ion-textarea>\n        </ion-item>\n\n        <ion-footer no-shadow no-border>\n            <ion-row>\n                <ion-col>\n                    <button ion-button full style="font-weight: bold;">Save</button>\n                </ion-col>\n                <ion-col>\n                    <button ion-button full type="submit" style="font-weight: bold;" (click)="nextPage();">Next</button>\n                </ion-col>\n            </ion-row>\n        </ion-footer>\n\n    </form>\n</ion-content>'/*ion-inline-end:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/remarks/remarks.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_10__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], RemarksPage);
    return RemarksPage;
}());

//# sourceMappingURL=remarks.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__remarks_remarks__ = __webpack_require__(19);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the WifiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ContentPage = /** @class */ (function () {
    function ContentPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.remarksPage = __WEBPACK_IMPORTED_MODULE_2__remarks_remarks__["a" /* RemarksPage */];
        this.loadProgress = 10;
    }
    ContentPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad ");
    };
    ContentPage.prototype.nextPage = function () {
        this.navCtrl.push(this.remarksPage, {
            page: 4,
            title: "Wifi and Airties"
        });
    };
    ContentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-content',template:/*ion-inline-start:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/content/content.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Content & Device</ion-title>\n    </ion-navbar>\n</ion-header>>\n\n<ion-content padding>\n\n    <progress-bar [progress]="loadProgress"></progress-bar>\n\n    <div>\n        <ion-label style="font-size: 24px; font-weight: bold;">Step 5:</ion-label>\n        <label style="font-size: 16px; font-weight: bold;">Inform about freebies and conduct product demo</label>\n    </div>\n\n    <div>\n        <br>\n        <label style="font-size: 18px; font-weight: bold;">Did the technician educate subscriber about</label>\n    </div>\n\n    <ion-card class="step-cards" (click)="toggleGreet();">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Freebies included in the plan i.e. Netflix, Hooq, Disney (if applicable)\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isGreet" *ngIf="isDisplayGreet">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <ion-card class="step-cards" (click)="toggleGreet();">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                How to activate (if applicable)\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isGreet" *ngIf="isDisplayGreet">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="NA" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <ion-card class="step-cards" (click)="toggleGreet();">\n\n            <ion-card-content>\n                <label no-lines no-padding style=" margin: 0;">\n                    <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                    Set up device and conduct demo i.e. Roku, Android TV, Chromecast (if applicable)\n                </label>\n            </ion-card-content>\n    \n            <ion-list radio-group [(ngModel)]="isGreet" *ngIf="isDisplayGreet">\n                <ion-row>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>Yes</ion-label>\n                            <ion-radio value="Yes" checked="true"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n    \n                    <ion-col>\n                        <ion-item>\n                            <ion-label>No</ion-label>\n                            <ion-radio value="No" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>N/A</ion-label>\n                            <ion-radio value="NA" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n            </ion-list>\n        </ion-card>\n\n    <div>\n        <br><br>\n        <label style="font-size: 18px; font-weight: bold;">Did the Senior Technician explain basic WiFi concepts using\n            WiFi MD?</label>\n    </div>\n\n    <ion-card class="step-cards" (click)="toggleGreet();">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Placement of router/ modem\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isGreet" *ngIf="isDisplayGreet">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n\n    <ion-card class="step-cards" (click)="toggleGreet();">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Factors affecting WiFi signal (thick walls, distance, appliances)\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isGreet" *ngIf="isDisplayGreet">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n    <ion-card class="step-cards" (click)="toggleGreet();">\n\n        <ion-card-content>\n            <label no-lines no-padding style=" margin: 0;">\n                <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                Did the Senior Technician offer Airties to maximize WiFi coverage?\n            </label>\n        </ion-card-content>\n\n        <ion-list radio-group [(ngModel)]="isGreet" *ngIf="isDisplayGreet">\n            <ion-row>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>Yes</ion-label>\n                        <ion-radio value="Yes" checked="true"></ion-radio>\n                    </ion-item>\n                </ion-col>\n\n                <ion-col>\n                    <ion-item>\n                        <ion-label>No</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n                <ion-col>\n                    <ion-item>\n                        <ion-label>N/A</ion-label>\n                        <ion-radio value="No" default></ion-radio>\n                    </ion-item>\n                </ion-col>\n            </ion-row>\n        </ion-list>\n    </ion-card>\n</ion-content>\n\n<ion-footer no-shadow no-border>\n    <ion-row>\n        <ion-col>\n            <button ion-button full style="font-weight: bold;">Save</button>\n        </ion-col>\n        <ion-col>\n            <button ion-button full style="font-weight: bold;" (click)="nextPage();">Next</button>\n        </ion-col>\n    </ion-row>\n</ion-footer>'/*ion-inline-end:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/content/content.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */]])
    ], ContentPage);
    return ContentPage;
}());

//# sourceMappingURL=content.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(229);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 229:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser_animations__ = __webpack_require__(285);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_components_progress_bar_progress_bar__ = __webpack_require__(287);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_component__ = __webpack_require__(288);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_survey_form_survey_form__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_steps_advance_call_advance_call__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_steps_intro_intro__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_steps_remarks_remarks__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_steps_installation_installation__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_steps_wifi_wifi__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_steps_content_content__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_steps_closing_closing__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_steps_overall_overall__ = __webpack_require__(109);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_steps_cfs_cfs__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_success_success__ = __webpack_require__(111);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_android_full_screen__ = __webpack_require__(289);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_components_progress_bar_progress_bar__["a" /* ProgressBarComponent */],
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_survey_form_survey_form__["a" /* SurveyPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_steps_advance_call_advance_call__["a" /* AdvanceCallPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_steps_intro_intro__["a" /* IntroPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_steps_remarks_remarks__["a" /* RemarksPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_steps_installation_installation__["a" /* InstallationPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_steps_wifi_wifi__["a" /* WifiPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_steps_content_content__["a" /* ContentPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_steps_closing_closing__["a" /* ClosingPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_steps_overall_overall__["a" /* OverallPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_steps_cfs_cfs__["a" /* CfsPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_success_success__["a" /* SuccessPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: 'pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: 'pages/splash/splash.module#SplashPageModule', name: 'SplashPage', segment: 'splash', priority: 'low', defaultHistory: [] },
                        { loadChildren: 'pages/steps/advance-call/advance-call.module#AdvanceCallPageModule', name: 'AdvanceCallPage', segment: 'advance-call', priority: 'low', defaultHistory: [] },
                        { loadChildren: 'pages/steps/cfs/cfs.module#CfsPageModule', name: 'CfsPage', segment: 'cfs', priority: 'low', defaultHistory: [] },
                        { loadChildren: 'pages/steps/closing/closing.module#ClosingPageModule', name: 'ClosingPage', segment: 'closing', priority: 'low', defaultHistory: [] },
                        { loadChildren: 'pages/steps/content/content.module#ContentPageModule', name: 'ContentPage', segment: 'content', priority: 'low', defaultHistory: [] },
                        { loadChildren: 'pages/steps/installation/installation.module#AdvanceCallPageModule', name: 'InstallationPage', segment: 'installation', priority: 'low', defaultHistory: [] },
                        { loadChildren: 'pages/steps/intro/intro.module#IntroPageModule', name: 'IntroPage', segment: 'intro', priority: 'low', defaultHistory: [] },
                        { loadChildren: 'pages/steps/overall/overall.module#OverallPageModule', name: 'OverallPage', segment: 'overall', priority: 'low', defaultHistory: [] },
                        { loadChildren: 'pages/steps/remarks/remarks.module#RemarkPageModule', name: 'RemarksPage', segment: 'remarks', priority: 'low', defaultHistory: [] },
                        { loadChildren: 'pages/steps/wifi/wifi.module#WifiPageModule', name: 'WifiPage', segment: 'wifi', priority: 'low', defaultHistory: [] },
                        { loadChildren: 'pages/success/success.module#SuccessPageModule', name: 'SuccessPage', segment: 'success', priority: 'low', defaultHistory: [] },
                        { loadChildren: 'pages/survey_form/survey_form.module#SurveyPageModule', name: 'SurveyPage', segment: 'survey_form', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_10__pages_steps_advance_call_advance_call__["a" /* AdvanceCallPage */],
                __WEBPACK_IMPORTED_MODULE_7__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_survey_form_survey_form__["a" /* SurveyPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_steps_intro_intro__["a" /* IntroPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_steps_remarks_remarks__["a" /* RemarksPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_steps_installation_installation__["a" /* InstallationPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_steps_wifi_wifi__["a" /* WifiPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_steps_content_content__["a" /* ContentPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_steps_closing_closing__["a" /* ClosingPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_steps_overall_overall__["a" /* OverallPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_steps_cfs_cfs__["a" /* CfsPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_success_success__["a" /* SuccessPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_19__angular_forms__["a" /* FormBuilder */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_android_full_screen__["a" /* AndroidFullScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 255:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RemarksEntity; });
var RemarksEntity = /** @class */ (function () {
    function RemarksEntity(whatWentWell, pointsOfImprovement) {
        this.whatWentWell = whatWentWell;
        this.pointsOfImprovement = pointsOfImprovement;
    }
    RemarksEntity.prototype.toJson = function () {
        return {
            "whatWentWell": this.whatWentWell,
            "pointsOfImprovement": this.pointsOfImprovement
        };
    };
    return RemarksEntity;
}());

//# sourceMappingURL=RemarksEntity.js.map

/***/ }),

/***/ 256:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdvanceCallEntity; });
var AdvanceCallEntity = /** @class */ (function () {
    function AdvanceCallEntity(dateOfVisit, timeOfVisit, newEta, advFees, smsSent, remarks) {
        this.dateOfVisit = dateOfVisit;
        this.timeOfVisit = timeOfVisit;
        this.newEta = newEta;
        this.advFees = advFees;
        this.smsSent = smsSent;
        this.remarks = remarks;
    }
    AdvanceCallEntity.prototype.toJson = function () {
        return {
            "advanceCall": {
                "dateOfVisit": this.dateOfVisit,
                "timeOfVisit": this.timeOfVisit,
                "newEta": this.newEta,
                "advFees": this.advFees,
                "smsSent": this.smsSent
            },
            "remarks": this.remarks.toJson()
        };
    };
    return AdvanceCallEntity;
}());

//# sourceMappingURL=AdvanceCallEntity.js.map

/***/ }),

/***/ 257:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurveyEntity; });
var SurveyEntity = /** @class */ (function () {
    function SurveyEntity() {
    }
    SurveyEntity.prototype.setCFSDetails = function (cfsDetails) {
        this.cfsDetails = cfsDetails;
    };
    SurveyEntity.prototype.setTechDetails = function (techDetails) {
        this.techDetails = techDetails;
    };
    SurveyEntity.prototype.setCustomerDetails = function (customerDetails) {
        this.customerDetails = customerDetails;
    };
    SurveyEntity.prototype.setAdvanceCall = function (advanceCallEntity) {
        this.advanceCallEntity = advanceCallEntity;
    };
    SurveyEntity.prototype.setIntro = function (introEntity) {
        this.introEntity = introEntity;
    };
    SurveyEntity.prototype.setInstallationEntity = function (installationEntity) {
        this.installationEntity = installationEntity;
    };
    SurveyEntity.prototype.setWifi = function (wifiEnitity) {
        this.wifiEnitity = wifiEnitity;
    };
    SurveyEntity.prototype.setContent = function (contentEntity) {
        this.contentEntity = contentEntity;
    };
    SurveyEntity.prototype.setClosing = function (closingEntity) {
        this.closingEntity = closingEntity;
    };
    SurveyEntity.prototype.setOverAll = function (overAllEntity) {
        this.overAllEntity = overAllEntity;
    };
    SurveyEntity.prototype.setCFSTechAttrib = function (cfsTechAttribute) {
        this.cfsTechAttribute = cfsTechAttribute;
    };
    SurveyEntity.prototype.toJson = function () {
        return JSON.stringify(this.surveyEntity = {
            "surveys": [
                {
                    "cfsDetails": this.cfsDetails.toJson(),
                    "technician": this.techDetails.toJson(),
                    "customerDetails": this.customerDetails.toJson(),
                    "step1": this.advanceCallEntity.toJson(),
                }
            ]
        });
    };
    return SurveyEntity;
}());

//# sourceMappingURL=SurveyEntity.js.map

/***/ }),

/***/ 258:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CFSDetailsEntity; });
var CFSDetailsEntity = /** @class */ (function () {
    function CFSDetailsEntity(details, observerName, observerID, dateOfObservation, contractor, region, foAreaHead, foTeamLeader) {
        this.details = details;
        this.observerName = observerName;
        this.observerID = observerID;
        this.dateOfObservation = dateOfObservation;
        this.contractor = contractor;
        this.region = region;
        this.foAreaHead = foAreaHead;
        this.foTeamLeader = foTeamLeader;
    }
    CFSDetailsEntity.prototype.toJson = function () {
        return {
            "details": this.details,
            "observerName": this.observerName,
            "observerID": this.observerID,
            "dateOfObservation": this.dateOfObservation,
            "contractor": this.contractor,
            "region": this.region,
            "foAreaHead": this.foAreaHead,
            "foTeamLeader": this.foTeamLeader
        };
    };
    return CFSDetailsEntity;
}());

//# sourceMappingURL=CFSDetailsEntity.js.map

/***/ }),

/***/ 259:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TechDetailsEntity; });
var TechDetailsEntity = /** @class */ (function () {
    function TechDetailsEntity(srTechName, srTechIdNum, techName, techIdNum) {
        this.srTechName = srTechName;
        this.srTechIdNum = srTechIdNum;
        this.techName = techName;
        this.techIdNum = techIdNum;
    }
    TechDetailsEntity.prototype.toJson = function () {
        return {
            "srTechName": this.srTechName,
            "srTechIdNum": this.srTechIdNum,
            "techName": this.srTechName,
            "techIdNum": this.techIdNum
        };
    };
    return TechDetailsEntity;
}());

//# sourceMappingURL=TechDetailsEntity.js.map

/***/ }),

/***/ 260:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomerDetailsEntity; });
var CustomerDetailsEntity = /** @class */ (function () {
    function CustomerDetailsEntity(subsName, serviceNo, workOrderRef, activity, location, repName, relToSubs) {
        this.subsName = subsName;
        this.serviceNo = serviceNo;
        this.workOrderRef = workOrderRef;
        this.activity = activity;
        this.location = location;
        this.repName = repName;
        this.relToSubs = relToSubs;
    }
    CustomerDetailsEntity.prototype.toJson = function () {
        return {
            "subsName": this.subsName,
            "serviceNo": this.serviceNo,
            "workOrderRef": this.workOrderRef,
            "activity": this.activity,
            "location": this.location,
            "repName": this.repName,
            "relToSubs": this.relToSubs
        };
    };
    return CustomerDetailsEntity;
}());

//# sourceMappingURL=CustomerDetailsEntity.js.map

/***/ }),

/***/ 287:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProgressBarComponent = /** @class */ (function () {
    function ProgressBarComponent() {
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('progress'),
        __metadata("design:type", Object)
    ], ProgressBarComponent.prototype, "progress", void 0);
    ProgressBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'progress-bar',template:/*ion-inline-start:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/components/progress-bar/progress-bar.html"*/'<div>\n    <div class="progress-outer">\n        <div class="progress-inner" [style.width]="progress + \'%\'"></div>\n    </div>\n    <label style="margin-left: 8px;">{{ progress }}% completed</label>\n</div>'/*ion-inline-end:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/components/progress-bar/progress-bar.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], ProgressBarComponent);
    return ProgressBarComponent;
}());

//# sourceMappingURL=progress-bar.js.map

/***/ }),

/***/ 288:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>'/*ion-inline-end:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SurveyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_animations__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__steps_advance_call_advance_call__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__data_entity_SurveyEntity__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__data_entity_CFSDetailsEntity__ = __webpack_require__(258);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__data_entity_TechDetailsEntity__ = __webpack_require__(259);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__data_entity_CustomerDetailsEntity__ = __webpack_require__(260);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the SurveyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SurveyPage = /** @class */ (function () {
    function SurveyPage(navCtrl, navParams, formBuilder, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.advanceCallPage = __WEBPACK_IMPORTED_MODULE_3__steps_advance_call_advance_call__["a" /* AdvanceCallPage */];
        this.isDisplayCFSDetails = true;
        this.isDisplayTechnicianDetails = false;
        this.isDisplayCustomerDetails = false;
        this.isValid = false;
        this.surveyEntity = new __WEBPACK_IMPORTED_MODULE_5__data_entity_SurveyEntity__["a" /* SurveyEntity */]();
        this.surveyForms = formBuilder.group({
            observerName: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            observerID: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            dateOfObservation: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].pattern],
            contractor: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            region: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            foAreaHead: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            foTeamLeader: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            details: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            srTechName: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            srTechIdNum: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            techName: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            techIdNum: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            subsName: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            serviceNo: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            workOrderRef: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            activity: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            location: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            nameOfRepre: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required],
            relationToSubs: ["", __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required]
        });
    }
    SurveyPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad ");
    };
    SurveyPage.prototype.onSubmit = function () {
        console.log(this.surveyForms.value);
        if (this.isDisplayCFSDetails) {
            if (!this.isValidCFSDetails()) {
                this.presentAlert();
                return;
            }
            var cfsDetails = new __WEBPACK_IMPORTED_MODULE_6__data_entity_CFSDetailsEntity__["a" /* CFSDetailsEntity */]("", this.surveyForms.value["observerName"], this.surveyForms.value["observerID"], this.surveyForms.value["dateOfObservation"], this.surveyForms.value["contractor"], this.surveyForms.value["region"], this.surveyForms.value["foAreaHead"], this.surveyForms.value["foTeamLeader"]);
            this.surveyEntity.setCFSDetails(cfsDetails);
        }
        if (this.isDisplayTechnicianDetails) {
            if (!this.isValidTechDetails()) {
                this.presentAlert();
                return;
            }
            var techDetails = new __WEBPACK_IMPORTED_MODULE_7__data_entity_TechDetailsEntity__["a" /* TechDetailsEntity */](this.surveyForms.value["srTechName"], this.surveyForms.value["srTechIdNum"], this.surveyForms.value["techName"], this.surveyForms.value["techIdNum"]);
            this.surveyEntity.setTechDetails(techDetails);
        }
        if (this.isDisplayCustomerDetails) {
            if (!this.isValidCustomerDetails()) {
                this.presentAlert();
                return;
            }
            var customerDetails = new __WEBPACK_IMPORTED_MODULE_8__data_entity_CustomerDetailsEntity__["a" /* CustomerDetailsEntity */](this.surveyForms.value["subsName"], this.surveyForms.value["serviceNo"], this.surveyForms.value["workOrderRef"], this.surveyForms.value["activity"], this.surveyForms.value["location"], this.surveyForms.value["nameOfRepre"], this.surveyForms.value["relationToSubs"]);
            this.surveyEntity.setCustomerDetails(customerDetails);
            this.navCtrl.push(this.advanceCallPage, {
                "surveyEntity": this.surveyEntity
            });
            return;
        }
        this.isDisplayCFSDetails = !this.isDisplayCFSDetails && this.isDisplayCustomerDetails;
        this.isDisplayTechnicianDetails = !this.isDisplayTechnicianDetails && !this.isDisplayCFSDetails;
        this.isDisplayCustomerDetails = !this.isDisplayCFSDetails && !this.isDisplayTechnicianDetails;
    };
    SurveyPage.prototype.isValidCFSDetails = function () {
        return (this.surveyForms.value["observerName"] != "" &&
            this.surveyForms.value["observerID"] != "" &&
            this.surveyForms.value["dateOfObservation"] != "" &&
            this.surveyForms.value["contractor"] != "" &&
            this.surveyForms.value["region"] != "" &&
            this.surveyForms.value["foAreaHead"] != "" &&
            this.surveyForms.value["foTeamLeader"] != "");
    };
    SurveyPage.prototype.isValidTechDetails = function () {
        return (this.surveyForms.value["srTechName"] != "" &&
            this.surveyForms.value["srTechIdNum"] != "" &&
            this.surveyForms.value["techName"] != "" &&
            this.surveyForms.value["techIdNum"] != "");
    };
    SurveyPage.prototype.isValidCustomerDetails = function () {
        return (this.surveyForms.value["subsName"] != "" &&
            this.surveyForms.value["serviceNo"] != "" &&
            this.surveyForms.value["workOrderRef"] != "" &&
            this.surveyForms.value["activity"] != "" &&
            this.surveyForms.value["location"] != "" &&
            this.surveyForms.value["nameOfRepre"] != "" &&
            this.surveyForms.value["relationToSubs"] != "");
    };
    SurveyPage.prototype.presentAlert = function () {
        var alert = this.alertCtrl.create({
            title: 'Warning',
            subTitle: 'Please input all required fields',
            buttons: ['Dismiss']
        });
        alert.present();
    };
    SurveyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-survey',template:/*ion-inline-start:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/survey_form/survey_form.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Advance call</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<form [formGroup]="surveyForms" (ngSubmit)="onSubmit()" style="height: 100%;">\n\n    <ion-content>\n        <br><br><br>\n        <ion-scroll padding scrollY="true">\n\n            <div *ngIf="isDisplayCFSDetails">\n                <label class="title">How\'s my team:</label><br>\n                <label class="title">Technician observer form</label><br><br>\n                <label style="font-size: 18px !important; font-weight: bold;">CFS Details</label>\n                <ion-item no-padding>\n                    <ion-label floating>Observer name</ion-label>\n                    <ion-input type="text" formControlName="observerName"></ion-input>\n                </ion-item>\n                <ion-item no-padding>\n                    <ion-label floating>Observer ID no</ion-label>\n                    <ion-input type="text" formControlName="observerID"></ion-input>\n                </ion-item>\n                <ion-item no-padding>\n                    <ion-label floating>Date of observation</ion-label>\n                    <ion-datetime type="text" formControlName="dateOfObservation"></ion-datetime>\n                    <ion-icon ios="md-calendar" md="md-calendar" item-right no-padding></ion-icon>\n                </ion-item>\n                <ion-item no-padding>\n                    <ion-label floating>Contractor</ion-label>\n                    <ion-select formControlName="contractor">\n                        <ion-option value="ADFLIX">ADFLIX</ion-option>\n                        <ion-option value="ADONAI">ADONAI</ion-option>\n                        <ion-option value="CHRISBEL">CHRISBEL</ion-option>\n                        <ion-option value="DUBLIN">DUBLIN</ion-option>\n                        <ion-option value="EDGECOMM">EDGECOMM</ion-option>\n                        <ion-option value="EMVIEM">EMVIEM</ion-option>\n                        <ion-option value="GOLDLINK">GOLDLINK</ion-option>\n                        <ion-option value="JAM">JAM</ion-option>\n                        <ion-option value="JASS">JASS</ion-option>\n                        <ion-option value="LEVINSON">LEVINSON</ion-option>\n                        <ion-option value="M-FORTUNE">M-FORTUNE</ion-option>\n                        <ion-option value="MG EXEO">MG EXEO</ion-option>\n                        <ion-option value="NG KHAI">NG KHAI</ion-option>\n                        <ion-option value="PHILKO UBINS">PHILKO UBINS</ion-option>\n                        <ion-option value="SMSP">SMSP</ion-option>\n                        <ion-option value="TESCO">TESCO</ion-option>\n                        <ion-option value="TRANSBIO">TRANSBIO</ion-option>\n                        <ion-option value="VISATECH">VISATECH</ion-option>\n                        <ion-option value="Others">Others</ion-option>\n                    </ion-select>\n                </ion-item>\n                <ion-item no-padding>\n                    <ion-label floating>Region</ion-label>\n                    <ion-select formControlName="region">\n                        <ion-option value="NCL">NCL</ion-option>\n                        <ion-option value="NGM">NGM</ion-option>\n                        <ion-option value="SGMA">SGMA</ion-option>\n                        <ion-option value="South Luzon">South Luzon</ion-option>\n                        <ion-option value="Visayas">Visayas</ion-option>\n                        <ion-option value="Mindanao">Mindanao</ion-option>\n                    </ion-select>\n                </ion-item>\n                <ion-item no-padding>\n                    <ion-label floating>Fo Area Head</ion-label>\n                    <ion-select formControlName="foAreaHead">\n                        <ion-option value="BARANDA,JOSELITO">BARANDA, JOSELITO</ion-option>\n                        <ion-option value="BASILIO,RODOLFO">BASILIO, RODOLFO</ion-option>\n                        <ion-option value="BORCENA,NELSON">BORCENA, NELSON</ion-option>\n                        <ion-option value="CASTILLO,RICHARD">CASTILLO, RICHARD</ion-option>\n                        <ion-option value="CORNELIO,RICO">CORNELIO, RICO</ion-option>\n                        <ion-option value="Cruz,LloydSherwin">Cruz, Lloyd Sherwin</ion-option>\n                        <ion-option value="GOLDDELROSARIO,DANTELINK">GOLDDEL ROSARIO, DANTELINK</ion-option>\n                        <ion-option value="DEL ROSARIO, DANTE">DEL ROSARIO, DANTE</ion-option>\n                        <ion-option value="ESTRADA, ARNEL">ESTRADA, ARNEL</ion-option>\n                        <ion-option value="LEVINSON">LEVINSON</ion-option>\n                        <ion-option value="FAJILAGUTAN, IAN">FAJILAGUTAN, IAN</ion-option>\n                        <ion-option value="FAUNILLAN, ROMEO">FAUNILLAN, ROMEO</ion-option>\n                        <ion-option value="GONZALES, JAIME">GONZALES, JAIME</ion-option>\n                        <ion-option value="PHILKO UBINS">PHILKO UBINS</ion-option>\n                        <ion-option value="IBARRA, JEFFREY">IBARRA, JEFFREY</ion-option>\n                        <ion-option value="LINDAYAO, ROMMEL">LINDAYAO, ROMMEL</ion-option>\n                        <ion-option value="LIWANAG, RONALDO">LIWANAG, RONALDO</ion-option>\n                        <ion-option value="MAPANAO, ANTHONY GARTH">MAPANAO, ANTHONY GARTH</ion-option>\n                        <ion-option value="MARQUEZ, IRVING">MARQUEZ, IRVING</ion-option>\n                        <ion-option value="OLAES, ALBERT">OLAES, ALBERT</ion-option>\n                        <ion-option value="OSORIO, CRISTOPHER">OSORIO, CRISTOPHER</ion-option>\n                        <ion-option value="ROJAS, MATIAS JR.">ROJAS, MATIAS JR.</ion-option>\n                        <ion-option value="ROSALES, NELSON">ROSALES, NELSON</ion-option>\n                        <ion-option value="TABORADA, GEROME">TABORADA, GEROME</ion-option>\n                        <ion-option value="TAMSE, JOHN ANTHONY">TAMSE, JOHN ANTHONY</ion-option>\n                        <ion-option value="OTHER">OTHER</ion-option>\n                    </ion-select>\n                </ion-item>\n                <ion-item no-padding>\n                    <ion-label floating>Fo Team Leader</ion-label>\n                    <ion-select formControlName="foTeamLeader">\n                        <ion-option value="AGOZAR, ALLAN">AGOZAR, ALLAN</ion-option>\n                        <ion-option value="ALANO, MICHAEL">ALANO, MICHAEL</ion-option>\n                        <ion-option value="ALEJANDRO, LEMUEL">ALEJANDRO, LEMUEL</ion-option>\n                        <ion-option value="AMORIN, MARLITO">AMORIN, MARLITO</ion-option>\n                        <ion-option value="AMULONG, FRANCISCO">AMULONG, FRANCISCO</ion-option>\n                        <ion-option value="BABASA, ALVIN">BABASA, ALVIN</ion-option>\n                        <ion-option value="BAÑARES, LIZA">BAÑARES, LIZA</ion-option>\n                        <ion-option value="BARDOS, RANEN">BARDOS, RANEN</ion-option>\n                        <ion-option value="BOLTRON, ERNEZAR">BOLTRON, ERNEZAR</ion-option>\n                        <ion-option value="BRAGAS, ELMER">BRAGAS, ELMER</ion-option>\n                        <ion-option value="BRONOLA, JERALD ERL">BRONOLA, JERALD ERL</ion-option>\n                        <ion-option value="CANDELARIO, REX">CANDELARIO, REX</ion-option>\n                        <ion-option value="CARLOBOS, JOSELITO">CARLOBOS, JOSELITO</ion-option>\n                        <ion-option value="CATAPANG, REYNALDO">CATAPANG, REYNALDO</ion-option>\n                        <ion-option value="CELESTIAL, JASHER">CELESTIAL, JASHER</ion-option>\n                        <ion-option value="CLAUDIO, JOSELITO">CLAUDIO, JOSELITO</ion-option>\n                        <ion-option value="CONCEPCION, RUBEN">CONCEPCION, RUBEN</ion-option>\n                        <ion-option value="COSTALES, CARLITO">COSTALES, CARLITO</ion-option>\n                        <ion-option value="CRUZ, DENNIS A.">CRUZ, DENNIS A.</ion-option>\n                        <ion-option value="CRUZ, DENNIS F.">CRUZ, DENNIS F.</ion-option>\n                        <ion-option value="CUERQUIS, MANUEL">CUERQUIS, MANUEL</ion-option>\n                        <ion-option value="CUERQUIS, MANUEL">DAHLEN, GARY</ion-option>\n                        <ion-option value="DARUCA, KENITH">DARUCA, KENITH</ion-option>\n                        <ion-option value="TABORADA, GEROME">TABORADA, GEROME</ion-option>\n                        <ion-option value="DAVID, DONALD">DAVID, DONALD</ion-option>\n                        <ion-option value="DE LA VEGA, RONALDO">DE LA VEGA, RONALDO</ion-option>\n\n                        <ion-option value="DELOS SANTOS, FRANCISCO">DELOS SANTOS, FRANCISCO</ion-option>\n                        <ion-option value="DIMACULANGAN, NOEL">DIMACULANGAN, NOEL</ion-option>\n                        <ion-option value="DOMANICO, KEITH">DOMANICO, KEITH</ion-option>\n\n                        <ion-option value="DONATO, JIMMY">DONATO, JIMMY</ion-option>\n                        <ion-option value="EBUENGA, ROMMEL">EBUENGA, ROMMEL</ion-option>\n                        <ion-option value="ERANA, MICHAEL">ERANA, MICHAEL</ion-option>\n\n                        <ion-option value="ESPIRITU, ANTHONY">ESPIRITU, ANTHONY</ion-option>\n                        <ion-option value="FALCON, MARY JANE">FALCON, MARY JANE</ion-option>\n                        <ion-option value="FERNANDEZ, RODRIGO JR.">FERNANDEZ, RODRIGO JR.</ion-option>\n\n                        <ion-option value="GAMBA, PURISIMO JR.">GAMBA, PURISIMO JR.</ion-option>\n                        <ion-option value="GAW, AIRON">GAW, AIRON</ion-option>\n                        <ion-option value="GO, MARLON">GO, MARLON</ion-option>\n\n                        <ion-option value="GOMEZ, RENER">GOMEZ, RENER</ion-option>\n                        <ion-option value="GUERZON, ADRIAN">GUERZON, ADRIAN</ion-option>\n                        <ion-option value="HERMOSO, MICHAEL">HERMOSO, MICHAEL</ion-option>\n\n                        <ion-option value="HERNANDEZ, JOSE">HERNANDEZ, JOSE</ion-option>\n                        <ion-option value="INAJADA, LUCIO">INAJADA, LUCIO</ion-option>\n                        <ion-option value="INOCANDO, EDWIN">INOCANDO, EDWIN</ion-option>\n\n                        <ion-option value="ISADA, JOHN MARIO">ISADA, JOHN MARIO</ion-option>\n                        <ion-option value="JABAAN, PEE JAY">JABAAN, PEE JAY</ion-option>\n                        <ion-option value="JAKOSALEM, SEGUNDO JR.">JAKOSALEM, SEGUNDO JR.</ion-option>\n\n                        <ion-option value="JAVIER, JAY">JAVIER, JAY</ion-option>\n                        <ion-option value="JERESANO, DARWIN">JERESANO, DARWIN</ion-option>\n                        <ion-option value="LASTIMOSA, ROY">LASTIMOSA, ROY</ion-option>\n\n                        <ion-option value="LAYOSA, NICOLAS Alex">LAYOSA, NICOLAS Alex</ion-option>\n                        <ion-option value="LAZARTE, DARRYL">LAZARTE, DARRYL</ion-option>\n                        <ion-option value="LIM, EDUARDO">LIM, EDUARDO</ion-option>\n\n                        <ion-option value="LLAVE, RUEL">LLAVE, RUEL</ion-option>\n                        <ion-option value="LONTOC, RONALDO">LONTOC, RONALDO</ion-option>\n                        <ion-option value="LUSTERIO, RODRIGO">LUSTERIO, RODRIGO</ion-option>\n\n                        <ion-option value="MABAQUIAO, NOEL">MABAQUIAO, NOEL</ion-option>\n                        <ion-option value="MACADAYA, JUNREY">MACADAYA, JUNREY</ion-option>\n                        <ion-option value="MACARAIG, ELVIN">MACARAIG, ELVIN</ion-option>\n\n                        <ion-option value="MACARANAS, JOSE">MACARANAS, JOSE</ion-option>\n                        <ion-option value="MALTO , JOHN DARRYL">MALTO , JOHN DARRYL</ion-option>\n                        <ion-option value="MARALIT JR., NELSON">MARALIT JR., NELSON</ion-option>\n\n                        <ion-option value="MENDOZA, ISAGANI">MENDOZA, ISAGANI</ion-option>\n                        <ion-option value="MERCADO, JOSE FREESERGS">MERCADO, JOSE FREESERGS</ion-option>\n                        <ion-option value="MONATO, RONNEL VICTOR">MONATO, RONNEL VICTOR</ion-option>\n\n                        <ion-option value="MONTALBO, ADRIANO GIL">MONTALBO, ADRIANO GIL</ion-option>\n                        <ion-option value="NULO, ZENAIDA">NULO, ZENAIDA</ion-option>\n                        <ion-option value="OLASIMAN, RANDY">OLASIMAN, RANDY</ion-option>\n\n                        <ion-option value="OPIANO, NEIL">OPIANO, NEIL</ion-option>\n                        <ion-option value="ORCULLO, GUILBERT">ORCULLO, GUILBERT</ion-option>\n                        <ion-option value="ORJALO, JONATHAN">ORJALO, JONATHAN</ion-option>\n\n                        <ion-option value="ORTEGA, DANILO">ORTEGA, DANILO</ion-option>\n                        <ion-option value="PAGUIRIGAN, ARNOLD">PAGUIRIGAN, ARNOLD</ion-option>\n                        <ion-option value="PANGAN, ALFONSO III">PANGAN, ALFONSO III</ion-option>\n\n                        <ion-option value="PERALTA, ANTHONY">PERALTA, ANTHONY</ion-option>\n                        <ion-option value="PERIA, EDWARDO">PERIA, EDWARDO</ion-option>\n                        <ion-option value="PUNO , ALLAN JOHN">PUNO , ALLAN JOHN</ion-option>\n\n                        <ion-option value="QUIDIP, POLICARPIO">QUIDIP, POLICARPIO</ion-option>\n                        <ion-option value="RACELIS, JASON">RACELIS, JASON</ion-option>\n                        <ion-option value="RAMIS, ANGELITO">RAMIS, ANGELITO</ion-option>\n\n                        <ion-option value="REBULTAN, ELMER">REBULTAN, ELMER</ion-option>\n                        <ion-option value="ROBIS, ALVIN">ROBIS, ALVIN</ion-option>\n                        <ion-option value="ROMARATE, JASON">ROMARATE, JASON</ion-option>\n\n                        <ion-option value="RONQUILLO, JOEL">RONQUILLO, JOEL</ion-option>\n                        <ion-option value="ROSAL, GODOFREDO JR.">ROSAL, GODOFREDO JR.</ion-option>\n                        <ion-option value="RUEDA, RAM KENNETH">RUEDA, RAM KENNETH</ion-option>\n\n                        <ion-option value="SADJE, WINNIE">SADJE, WINNIE</ion-option>\n                        <ion-option value="SANCHEZ, ARNOLD">SANCHEZ, ARNOLD</ion-option>\n                        <ion-option value="SEGOVIA, MARK">SEGOVIA, MARK</ion-option>\n\n                        <ion-option value="SEVILLA, DARWIN">SEVILLA, DARWIN</ion-option>\n                        <ion-option value="SORREL, NORMAN">SORREL, NORMAN</ion-option>\n                        <ion-option value="TALINGTING , ARIEL">TALINGTING , ARIEL</ion-option>\n\n                        <ion-option value="TARLIT , ARGEL">TARLIT , ARGEL</ion-option>\n                        <ion-option value="TAYAG, RUBEN">TAYAG, RUBEN</ion-option>\n                        <ion-option value="PUNO , ALLAN JOHN">PUNO , ALLAN JOHN</ion-option>\n\n                        <ion-option value="TEJANO, MARY ANN">TEJANO, MARY ANN</ion-option>\n                        <ion-option value="TEODORO, ARTHUR">TEODORO, ARTHUR</ion-option>\n                        <ion-option value="TOLEDO, EULOGIO">TOLEDO, EULOGIO</ion-option>\n\n                        <ion-option value="TEJATOLEDO, RENATO">TEJATOLEDO, RENATO</ion-option>\n                        <ion-option value="TORRECAMPO, JOEL">TORRECAMPO, JOEL</ion-option>\n                        <ion-option value="TORRES, LOUIE">TORRES, LOUIE</ion-option>\n\n                        <ion-option value="TUBAL , MICHAEL">TUBAL , MICHAEL</ion-option>\n                        <ion-option value="USON, MICHAEL PATRICK">USON, MICHAEL PATRICK</ion-option>\n                        <ion-option value="VERDAD, ARNOLD">VERDAD, ARNOLD</ion-option>\n\n                        <ion-option value="VILLARUZ, KENN JOHN">VILLARUZ, KENN JOHN</ion-option>\n                        <ion-option value="YMAS, JONELLE">YMAS, JONELLE</ion-option>\n                        <ion-option value="YU , DONN EDWARD">YU , DONN EDWARD</ion-option>\n                        <ion-option value="OTHER">OTHER</ion-option>\n                    </ion-select>\n                </ion-item>\n\n            </div>\n\n            <!-- </form> -->\n\n            <div *ngIf="isDisplayTechnicianDetails">\n                <ion-label style="font-size: 18px !important; font-weight: bold;">Technician Details</ion-label>\n                <ion-item no-padding>\n                    <ion-label floating>Description (optional)</ion-label>\n                    <ion-input type="text" formControlName="details"></ion-input>\n                </ion-item>\n                <ion-item no-padding>\n                    <ion-label floating>SR Technician Name</ion-label>\n                    <ion-input type="text" formControlName="srTechName"></ion-input>\n                </ion-item>\n                <ion-item no-padding>\n                    <ion-label floating>SR Technician ID No.</ion-label>\n                    <ion-input type="text" formControlName="srTechIdNum"></ion-input>\n                </ion-item>\n\n                <ion-item no-padding>\n                    <ion-label floating>Technician name</ion-label>\n                    <ion-input type="text" formControlName="techName"></ion-input>\n                </ion-item>\n                <ion-item no-padding>\n                    <ion-label floating>Technician ID no.</ion-label>\n                    <ion-input type="text" formControlName="techIdNum"></ion-input>\n                </ion-item>\n            </div>\n\n            <div *ngIf="isDisplayCustomerDetails">\n                <ion-label style="font-size: 18px !important; font-weight: bold;">Customer Details</ion-label>\n                <ion-item no-padding>\n                    <ion-label floating>Subscriber name</ion-label>\n                    <ion-input type="text" formControlName="subsName"></ion-input>\n                </ion-item>\n                <ion-item no-padding>\n                    <ion-label floating>Service no.</ion-label>\n                    <ion-input type="text" formControlName="serviceNo"></ion-input>\n                </ion-item>\n                <ion-item no-padding>\n                    <ion-label floating>Work order reference</ion-label>\n                    <ion-input type="text" formControlName="workOrderRef"></ion-input>\n                </ion-item>\n                <br />\n                <ion-col no-padding radio-group formControlName="activity">\n                    <ion-row>\n                        <ion-label style="font-size: 14px !important; font-weight: bold;">Activity</ion-label>\n                    </ion-row>\n                    <ion-row no-padding>\n                        <ion-col>\n                            <ion-item no-padding>\n                                <ion-label>Install</ion-label>\n                                <ion-radio value="install"></ion-radio>\n                            </ion-item>\n                        </ion-col>\n                        <ion-col>\n                            <ion-item no-padding>\n                                <ion-label>Repair</ion-label>\n                                <ion-radio value="repair"></ion-radio>\n                            </ion-item>\n                        </ion-col>\n                    </ion-row>\n                </ion-col>\n                <ion-item no-padding>\n                    <ion-label floating>Location (City)</ion-label>\n                    <ion-input type="text" formControlName="location"></ion-input>\n                </ion-item>\n                <ion-item no-padding>\n                    <ion-label floating>If subscriber is not present, indicate name of representative</ion-label>\n                    <ion-input type="text" formControlName="nameOfRepre"></ion-input>\n                </ion-item>\n                <ion-item no-padding>\n                    <ion-label floating>Relation to subscriber</ion-label>\n                    <ion-input type="text" formControlName="relationToSubs"></ion-input>\n                </ion-item>\n            </div>\n\n        </ion-scroll>\n    </ion-content>\n\n    <ion-footer no-shadow no-border>\n        <ion-row>\n            <ion-col>\n                <button ion-button full style="font-weight: bold;" [navPush]="advanceCallPage">Save</button>\n            </ion-col>\n            <ion-col>\n                <button ion-button full type="submit" style="font-weight: bold;">Next</button>\n            </ion-col>\n        </ion-row>\n    </ion-footer>\n\n</form>'/*ion-inline-end:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/survey_form/survey_form.html"*/,
            animations: [
                Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["j" /* trigger */])('visibilityChanged', [
                    Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["g" /* state */])('shown', Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["h" /* style */])({ opacity: 1 })),
                    Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["g" /* state */])('hidden', Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["h" /* style */])({ opacity: 0 })),
                    Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["i" /* transition */])('* => *', Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["e" /* animate */])('500ms'))
                ])
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], SurveyPage);
    return SurveyPage;
}());

//# sourceMappingURL=survey_form.js.map

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IntroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__remarks_remarks__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { AdvanceCallPage } from '../steps/advance-call/advance-call';
/**
 * Generated class for the SurveyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var IntroPage = /** @class */ (function () {
    function IntroPage(navCtrl, navParams, formBuilder, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.remarksPage = __WEBPACK_IMPORTED_MODULE_2__remarks_remarks__["a" /* RemarksPage */];
        this.loadProgress = 10;
        this.isDisplayGreet = false;
        this.isDisplayPresentID = false;
        this.isDisplaySayName = false;
        this.isDisplayPlan = false;
        this.isDisplayDataAllowance = false;
        this.isDisplayCollection = false;
        this.isDisplayFreebies = false;
        this.surveyEntity = navParams.get("surveyEntity");
        this.surveyForms = formBuilder.group({
            "isTechnicianArrived": ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            "isGreet": ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            "isPresentID": ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            "isSayName": ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            "isTechVisit": ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            "isPlan": ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            "isDataAllowance": ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            "isCollection": ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            "isFreebies": ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
        });
    }
    IntroPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad ");
    };
    IntroPage.prototype.onSubmit = function () {
        this.navCtrl.push(this.remarksPage, {
            page: 2,
            title: "Intro",
            surveyEntity: this.surveyEntity
        });
    };
    IntroPage.prototype.previousPage = function () {
    };
    /**
     *  TODO: recode
     */
    IntroPage.prototype.toggleGreet = function () {
        this.isDisplayGreet = !this.isDisplayGreet;
        this.isDisplayPresentID = false;
        this.isDisplaySayName = false;
    };
    IntroPage.prototype.togglePresentID = function () {
        this.isDisplayGreet = false;
        this.isDisplayPresentID = !this.isDisplayPresentID;
        this.isDisplaySayName = false;
    };
    IntroPage.prototype.toggleSayName = function () {
        this.isDisplayGreet = false;
        this.isDisplayPresentID = false;
        this.isDisplaySayName = !this.isDisplaySayName;
    };
    //
    IntroPage.prototype.togglePlan = function () {
        this.isDisplayPlan = !this.isDisplayPlan;
        this.isDisplayDataAllowance = false;
        this.isDisplayCollection = false;
        this.isDisplayFreebies = false;
    };
    IntroPage.prototype.toggleData = function () {
        this.isDisplayPlan = false;
        this.isDisplayDataAllowance = !this.isDisplayDataAllowance;
        this.isDisplayCollection = false;
        this.isDisplayFreebies = false;
    };
    IntroPage.prototype.toggleCollection = function () {
        this.isDisplayPlan = false;
        this.isDisplayDataAllowance = false;
        this.isDisplayCollection = !this.isDisplayCollection;
        this.isDisplayFreebies = false;
    };
    IntroPage.prototype.toggleFreebies = function () {
        this.isDisplayPlan = false;
        this.isDisplayDataAllowance = false;
        this.isDisplayCollection = false;
        this.isDisplayFreebies = !this.isDisplayFreebies;
    };
    IntroPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-intro',template:/*ion-inline-start:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/intro/intro.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Intro</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-content padding>\n\n    <progress-bar [progress]="loadProgress"></progress-bar>\n\n    <div>\n        <br><br>\n        <label style="font-size: 16px; color:grey; margin-top: 16px;">How\'s my team: </label>\n        <label style="font-size: 16px;">Technician observation form</label>\n        <ion-label style="font-size: 24px; font-weight: bold;">Step 2:</ion-label>\n\n        <label style="font-size: 18px; font-weight: bold;">Establishing identity and confirmation of Globe services\n            availed of</label>\n    </div>\n\n    <form [formGroup]="surveyForms" (ngSubmit)="onSubmit()">\n\n        <ion-card class="step-cards">\n\n            <ion-card-content>\n                <label no-lines no-padding style=" margin: 0;">\n                    <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                    Did the technician arrive based on the expected time of appointment?\n                </label>\n            </ion-card-content>\n\n            <ion-list radio-group formControlName="isTechnicianArrived">\n                <ion-row>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>Yes</ion-label>\n                            <ion-radio value="Yes" checked="true"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>No</ion-label>\n                            <ion-radio value="No" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n            </ion-list>\n        </ion-card>\n\n        <div>\n            <br><br>\n            <label style="font-size: 18px; font-weight: bold;">Did the Senior technician introduce the team properly by</label>\n        </div>\n\n        <ion-card class="step-cards" (click)="toggleGreet();">\n\n            <ion-card-content>\n                <label no-lines no-padding style=" margin: 0;">\n                    <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                    Greeting the subscriber\n                </label>\n            </ion-card-content>\n\n            <ion-list radio-group formControlName="isGreet" *ngIf="isDisplayGreet">\n                <ion-row>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>Yes</ion-label>\n                            <ion-radio value="Yes" checked="true"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>No</ion-label>\n                            <ion-radio value="No" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n            </ion-list>\n        </ion-card>\n\n        <ion-card class="step-cards" (click)="togglePresentID();">\n\n            <ion-card-content id="first">\n                <div class="item item-text-wrap">\n                    <label no-lines no-padding style="margin: 0;">\n                        <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                        Presenting his Globe accredited ID\n                    </label>\n                </div>\n            </ion-card-content>\n\n            <ion-list radio-group formControlName="isPresentID" *ngIf="isDisplayPresentID">\n                <ion-row>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>Yes</ion-label>\n                            <ion-radio value="Yes" checked="true"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>No</ion-label>\n                            <ion-radio value="No" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n            </ion-list>\n        </ion-card>\n\n        <ion-card class="step-cards" (click)="toggleSayName();">\n\n            <ion-card-content id="first">\n                <div class="item item-text-wrap">\n                    <label no-lines no-padding style="margin: 0;">\n                        <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                        Saying his first and last name and his teammate\'s\n                    </label>\n                </div>\n            </ion-card-content>\n\n            <ion-list radio-group formControlName="isSayName" *ngIf="isDisplaySayName">\n                <ion-row>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>Yes</ion-label>\n                            <ion-radio value="Yes" checked="true"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>No</ion-label>\n                            <ion-radio value="No" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n            </ion-list>\n        </ion-card>\n\n        <div>\n            <br><br>\n            <label style="font-size: 18px; font-weight: bold;">Did the technician clearly state the purpose of the\n                visit?</label>\n        </div>\n\n        <ion-card class="step-cards">\n\n            <ion-list radio-group formControlName="isTechVisit">\n                <ion-row>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>Yes</ion-label>\n                            <ion-radio value="Yes" checked="true"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>No</ion-label>\n                            <ion-radio value="No" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n            </ion-list>\n        </ion-card>\n\n        <div>\n            <br><br>\n            <label style="font-size: 18px; font-weight: bold;">Did the Senior technician confirm the Globe services\n                availed\n                of</label>\n        </div>\n\n        <ion-card class="step-cards" (click)="togglePlan();">\n\n            <ion-card-content>\n                <label no-lines no-padding style=" margin: 0;">\n                    <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                    Plan and Monthly Service Fee\n                </label>\n            </ion-card-content>\n\n            <ion-list radio-group formControlName="isPlan" *ngIf="isDisplayPlan">\n                <ion-row>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>Yes</ion-label>\n                            <ion-radio value="Yes" checked="true"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>No</ion-label>\n                            <ion-radio value="No" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>N/A</ion-label>\n                            <ion-radio value="NA" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n            </ion-list>\n        </ion-card>\n\n        <ion-card class="step-cards" (click)="toggleData();">\n\n            <ion-card-content id="first">\n                <div class="item item-text-wrap">\n                    <label no-lines no-padding style="margin: 0;">\n                        <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                        Data allowance\n                    </label>\n                </div>\n            </ion-card-content>\n\n            <ion-list radio-group formControlName="isDataAllowance" *ngIf="isDisplayDataAllowance">\n                <ion-row>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>Yes</ion-label>\n                            <ion-radio value="Yes" checked="true"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>No</ion-label>\n                            <ion-radio value="No" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>N/A</ion-label>\n                            <ion-radio value="NA" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n            </ion-list>\n        </ion-card>\n\n        <ion-card class="step-cards" (click)="toggleCollection();">\n\n            <ion-card-content id="first">\n                <div class="item item-text-wrap">\n                    <label no-lines no-padding style="margin: 0;">\n                        <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                        Fees for collection and/or charge to bill (as applicable)\n                    </label>\n                </div>\n            </ion-card-content>\n\n            <ion-list radio-group formControlName="isCollection" *ngIf="isDisplayCollection">\n                <ion-row>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>Yes</ion-label>\n                            <ion-radio value="Yes" checked="true"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>No</ion-label>\n                            <ion-radio value="No" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>N/A</ion-label>\n                            <ion-radio value="NA" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n            </ion-list>\n        </ion-card>\n\n        <ion-card class="step-cards" (click)="toggleFreebies();">\n\n            <ion-card-content id="first">\n                <div class="item item-text-wrap">\n                    <label no-lines no-padding style="margin: 0;">\n                        <ion-icon ios="ios-arrow-dropdown" md="md-arrow-dropdown" class="step-icons"></ion-icon>\n                        Freebies (i.e. Netflix, HOOQ, Disney, etc.) (as applicable)\n                    </label>\n                </div>\n            </ion-card-content>\n\n            <ion-list radio-group formControlName="isFreebies" *ngIf="isDisplayFreebies">\n                <ion-row>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>Yes</ion-label>\n                            <ion-radio value="Yes" checked="true"></ion-radio>\n                        </ion-item>\n                    </ion-col>\n\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>No</ion-label>\n                            <ion-radio value="No" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                    <ion-col>\n                        <ion-item>\n                            <ion-label>N/A</ion-label>\n                            <ion-radio value="NA" default></ion-radio>\n                        </ion-item>\n                    </ion-col>\n                </ion-row>\n            </ion-list>\n        </ion-card>\n\n        <ion-footer no-shadow no-border>\n            <ion-row>\n                <ion-col>\n                    <button ion-button full style="font-weight: bold;">Save</button>\n                </ion-col>\n                <ion-col>\n                    <button ion-button full style="font-weight: bold;">Next</button>\n                </ion-col>\n            </ion-row>\n        </ion-footer>\n\n    </form>\n</ion-content>'/*ion-inline-end:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/steps/intro/intro.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], IntroPage);
    return IntroPage;
}());

//# sourceMappingURL=intro.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_animations__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__survey_form_survey_form__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the SplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, formBuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.surveyPage = __WEBPACK_IMPORTED_MODULE_3__survey_form_survey_form__["a" /* SurveyPage */];
        this.splash = true;
        this.visibility = 'hidden';
        this.formGroup = formBuilder.group({
            name: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["f" /* Validators */].required]
        });
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log("ionViewDidLoad ");
        setTimeout(function () {
            _this.splash = false;
            console.log(_this.splash);
        }, 4000);
    };
    LoginPage.prototype.onSubmit = function () {
        if (this.formGroup.valid) {
            this.navCtrl.push(this.surveyPage);
        }
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/login/login.html"*/'<ion-content padding *ngIf="splash" id="custom-overlay">\n    <ion-grid style="height: 100%">\n        <ion-row justify-content-center align-items-center style="height: 100%">\n            <img src="assets/imgs/globe_telecom_logo_detail.png" width="250" class="imgSplash" />\n        </ion-row>\n        <ion-row justify-content-center align-items-center padding>\n            <label style="color: grey">app version 0.0.1</label>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n\n<ion-content *ngIf="!splash">\n\n    <ion-row style="padding-top: 32px;" justify-content-center align-items-center>\n        <img src="assets/imgs/globe_telecom_logo_detail.png" width="250" class="imgSplash" />\n    </ion-row>\n\n    <form padding [formGroup]="formGroup" (ngSubmit)="onSubmit()" style="margin-top:32px;">\n\n        <label class="title">Welcome to</label><br>\n        <label class="title">the survery application</label>\n        <ion-item no-padding>\n            <ion-label floating>Enter your fullname</ion-label>\n            <ion-input type="text" class="text-input" formControlName="name"></ion-input>\n        </ion-item>\n\n        <ion-footer no-shadow no-border no-padding>\n            <ion-row justify-content-center align-items-center padding>\n                <label style="color: grey">app version 0.0.1</label>\n            </ion-row>\n            <button ion-button full type="submit" class="btn-submit" [disabled]="!formGroup.valid">Start\n                Survey</button>\n        </ion-footer>\n    </form>\n\n</ion-content>'/*ion-inline-end:"/Users/geraldtayag/Desktop/Projects/ionic/globe_mobile_survey/src/app/pages/login/login.html"*/,
            animations: [
                Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["j" /* trigger */])('visibilityChanged', [
                    Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["g" /* state */])('shown', Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["h" /* style */])({ opacity: 1 })),
                    Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["g" /* state */])('hidden', Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["h" /* style */])({ opacity: 0 })),
                    Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["i" /* transition */])('* => *', Object(__WEBPACK_IMPORTED_MODULE_2__angular_animations__["e" /* animate */])('500ms'))
                ])
            ]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */], __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormBuilder */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ })

},[206]);
//# sourceMappingURL=main.js.map