import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { trigger, state, style, animate, transition } from '@angular/animations';

import { AdvanceCallPage } from '../steps/advance-call/advance-call';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SurveyEntity } from '../../data/entity/SurveyEntity';
import { CFSDetailsEntity } from '../../data/entity/CFSDetailsEntity';
import { TechDetailsEntity } from '../../data/entity/TechDetailsEntity';
import { CustomerDetailsEntity } from '../../data/entity/CustomerDetailsEntity';

/**
 * Generated class for the SurveyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-survey',
    templateUrl: 'survey_form.html',
    animations: [
        trigger('visibilityChanged', [
            state('shown', style({ opacity: 1 })),
            state('hidden', style({ opacity: 0 })),
            transition('* => *', animate('500ms'))
        ])
    ]
})
export class SurveyPage {

    public advanceCallPage = AdvanceCallPage;
    public isDisplayCFSDetails = true;
    public isDisplayTechnicianDetails = false;
    public isDisplayCustomerDetails = false;

    public surveyForms: FormGroup;

    public isValid = false;

    private surveyEntity = new SurveyEntity();

    constructor(public navCtrl: NavController, public navParams: NavParams,
        private formBuilder: FormBuilder, private alertCtrl: AlertController) {

        this.surveyForms = formBuilder.group({
            observerName: ["", Validators.required],
            observerID: ["", Validators.required],
            dateOfObservation: ["", Validators.pattern],
            contractor: ["", Validators.required],
            region: ["", Validators.required],
            foAreaHead: ["", Validators.required],
            foTeamLeader: ["", Validators.required],
            details: ["", Validators.required],
            srTechName: ["", Validators.required],
            srTechIdNum: ["", Validators.required],
            techName: ["", Validators.required],
            techIdNum: ["", Validators.required],
            subsName: ["", Validators.required],
            serviceNo: ["", Validators.required],
            workOrderRef: ["", Validators.required],
            activity: ["", Validators.required],
            location: ["", Validators.required],
            nameOfRepre: ["", Validators.required],
            relationToSubs: ["", Validators.required]
        });
    }

    ionViewDidLoad() {
        console.log("ionViewDidLoad ");
    }

    public onSubmit() {

        console.log(this.surveyForms.value);

        if (this.isDisplayCFSDetails) {
            if (!this.isValidCFSDetails()) {
                this.presentAlert();
                return;
            }

            var cfsDetails = new CFSDetailsEntity("", this.surveyForms.value["observerName"],
                this.surveyForms.value["observerID"],
                this.surveyForms.value["dateOfObservation"],
                this.surveyForms.value["contractor"],
                this.surveyForms.value["region"],
                this.surveyForms.value["foAreaHead"],
                this.surveyForms.value["foTeamLeader"]);

            this.surveyEntity.setCFSDetails(cfsDetails);
        }

        if (this.isDisplayTechnicianDetails) {
            if (!this.isValidTechDetails()) {
                this.presentAlert();
                return;
            }

            var techDetails = new TechDetailsEntity(this.surveyForms.value["srTechName"],
                this.surveyForms.value["srTechIdNum"],
                this.surveyForms.value["techName"],
                this.surveyForms.value["techIdNum"]);

            this.surveyEntity.setTechDetails(techDetails);
        }

        if (this.isDisplayCustomerDetails) {
            if (!this.isValidCustomerDetails()) {
                this.presentAlert();
                return;
            }

            var customerDetails = new CustomerDetailsEntity(this.surveyForms.value["subsName"],
                this.surveyForms.value["serviceNo"],
                this.surveyForms.value["workOrderRef"],
                this.surveyForms.value["activity"],
                this.surveyForms.value["location"],
                this.surveyForms.value["nameOfRepre"],
                this.surveyForms.value["relationToSubs"])

            this.surveyEntity.setCustomerDetails(customerDetails);

            this.navCtrl.push(this.advanceCallPage, {
                "surveyEntity" : this.surveyEntity
            });

            return;
        }

        this.isDisplayCFSDetails = !this.isDisplayCFSDetails && this.isDisplayCustomerDetails;
        this.isDisplayTechnicianDetails = !this.isDisplayTechnicianDetails && !this.isDisplayCFSDetails;
        this.isDisplayCustomerDetails = !this.isDisplayCFSDetails && !this.isDisplayTechnicianDetails;

    }

    private isValidCFSDetails(): boolean {
        return (this.surveyForms.value["observerName"] != "" &&
            this.surveyForms.value["observerID"] != "" &&
            this.surveyForms.value["dateOfObservation"] != "" &&
            this.surveyForms.value["contractor"] != "" &&
            this.surveyForms.value["region"] != "" &&
            this.surveyForms.value["foAreaHead"] != "" &&
            this.surveyForms.value["foTeamLeader"] != ""
        );
    }

    private isValidTechDetails(): boolean {
        return (this.surveyForms.value["srTechName"] != "" &&
            this.surveyForms.value["srTechIdNum"] != "" &&
            this.surveyForms.value["techName"] != "" &&
            this.surveyForms.value["techIdNum"] != ""
        );
    }

    private isValidCustomerDetails(): boolean {
        return (this.surveyForms.value["subsName"] != "" &&
            this.surveyForms.value["serviceNo"] != "" &&
            this.surveyForms.value["workOrderRef"] != "" &&
            this.surveyForms.value["activity"] != "" &&
            this.surveyForms.value["location"] != "" &&
            this.surveyForms.value["nameOfRepre"] != "" &&
            this.surveyForms.value["relationToSubs"] != ""
        );
    }

    presentAlert() {
        let alert = this.alertCtrl.create({
            title: 'Warning',
            subTitle: 'Please input all required fields',
            buttons: ['Dismiss']
        });
        alert.present();
    }

}
