import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { IntroPage } from '../intro/intro';
import { RemarksPage } from '../remarks/remarks';


/**
 * Generated class for the SplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-installation',
    templateUrl: 'installation.html',
})
export class InstallationPage {

    public introPage = IntroPage;
    public remarksPage = RemarksPage;
    loadProgress: number = 30;

    constructor(public navCtrl: NavController, public navParams: NavParams) { }

    ionViewDidLoad() {
        console.log("ionViewDidLoad ");
    }

    public nextPage(){
        this.navCtrl.push(this.remarksPage, {
            page: 3,
            title: "Installation/Repair"
        });
    }
}
