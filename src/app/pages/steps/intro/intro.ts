import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { RemarksPage } from '../remarks/remarks';
import { SurveyEntity } from '../../../data/entity/SurveyEntity';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// import { AdvanceCallPage } from '../steps/advance-call/advance-call';

/**
 * Generated class for the SurveyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-intro',
    templateUrl: 'intro.html',
})
export class IntroPage {

    remarksPage = RemarksPage;
    loadProgress: number = 10;

    isDisplayGreet: boolean = false;
    isDisplayPresentID: boolean = false;
    isDisplaySayName: boolean = false;

    isDisplayPlan: boolean = false;
    isDisplayDataAllowance: boolean = false;
    isDisplayCollection: boolean = false;
    isDisplayFreebies: boolean = false;

    private surveyEntity: SurveyEntity;

    public surveyForms: FormGroup;

    constructor(public navCtrl: NavController, public navParams: NavParams,
        private formBuilder: FormBuilder, private alertCtrl: AlertController) {
        this.surveyEntity = navParams.get("surveyEntity");
        this.surveyForms = formBuilder.group({
            "isTechnicianArrived" : ["", Validators.required],
            "isGreet" : ["", Validators.required],
            "isPresentID" : ["", Validators.required],
            "isSayName" : ["", Validators.required],
            "isTechVisit" : ["", Validators.required],
            "isPlan" : ["", Validators.required],
            "isDataAllowance" : ["", Validators.required],
            "isCollection" : ["", Validators.required],
            "isFreebies" : ["", Validators.required],
        });
    }

    ionViewDidLoad() {
        console.log("ionViewDidLoad ");
    }

    public onSubmit() {
        this.navCtrl.push(this.remarksPage, {
            page: 2,
            title: "Intro",
            surveyEntity: this.surveyEntity
        });
    }

    public previousPage() {

    }

    /**
     *  TODO: recode
     */
    public toggleGreet() {
        this.isDisplayGreet = !this.isDisplayGreet;
        this.isDisplayPresentID = false;
        this.isDisplaySayName = false;
    }

    public togglePresentID() {
        this.isDisplayGreet = false;
        this.isDisplayPresentID = !this.isDisplayPresentID;
        this.isDisplaySayName = false;
    }

    public toggleSayName() {
        this.isDisplayGreet = false;
        this.isDisplayPresentID = false;
        this.isDisplaySayName = !this.isDisplaySayName;
    }

    //
    public togglePlan() {
        this.isDisplayPlan = !this.isDisplayPlan;
        this.isDisplayDataAllowance = false;
        this.isDisplayCollection = false;
        this.isDisplayFreebies = false;
    }

    public toggleData() {
        this.isDisplayPlan = false;
        this.isDisplayDataAllowance = !this.isDisplayDataAllowance;
        this.isDisplayCollection = false;
        this.isDisplayFreebies = false;
    }

    public toggleCollection() {
        this.isDisplayPlan = false;
        this.isDisplayDataAllowance = false;
        this.isDisplayCollection = !this.isDisplayCollection;
        this.isDisplayFreebies = false;
    }

    public toggleFreebies() {
        this.isDisplayPlan = false;
        this.isDisplayDataAllowance = false;
        this.isDisplayCollection = false;
        this.isDisplayFreebies = !this.isDisplayFreebies;
    }
}
