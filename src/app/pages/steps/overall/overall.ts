import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { RemarksPage } from '../remarks/remarks';

// import { AdvanceCallPage } from '../steps/advance-call/advance-call';

/**
 * Generated class for the SurveyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-overall',
    templateUrl: 'overall.html',
})
export class OverallPage {

    remarksPage = RemarksPage;
    loadProgress: number = 10;

    isDisplayGreet: boolean = false;
    isDisplayPresentID: boolean = false;
    isDisplaySayName: boolean = false;

    constructor(public navCtrl: NavController, public navParams: NavParams) { }

    ionViewDidLoad() {
        console.log("ionViewDidLoad ");
    }

    public nextPage() {
        this.navCtrl.push(this.remarksPage, {
            page: 6,
            title: "Overall"
        });
    }

    public previousPage() {

    }

    /**
     *  TODO: recode
     */
    public toggleGreet(){
        this.isDisplayGreet = !this.isDisplayGreet;
        this.isDisplayPresentID = false;
        this.isDisplaySayName = false;
    }

    public togglePresentID(){
        this.isDisplayGreet = false;
        this.isDisplayPresentID = !this.isDisplayPresentID;
        this.isDisplaySayName = false; 
    }

    public toggleSayName(){
        this.isDisplayGreet = false;
        this.isDisplayPresentID = false;
        this.isDisplaySayName = !this.isDisplaySayName;
    }
}
