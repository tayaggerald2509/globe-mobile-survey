import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { RemarksPage } from '../remarks/remarks';

/**
 * Generated class for the WifiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-content',
    templateUrl: 'content.html',
})
export class ContentPage {

    public remarksPage = RemarksPage;
    
    loadProgress: number = 10;

    public title: String;
    public page: number;

    constructor(public navCtrl: NavController, public navParams: NavParams) {
        
    }

    ionViewDidLoad() {
        console.log("ionViewDidLoad ");
    }

    public nextPage() {
        this.navCtrl.push(this.remarksPage, {
            page: 4,
            title: "Wifi and Airties"
        });
    }

}
