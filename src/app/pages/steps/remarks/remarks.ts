import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { IntroPage } from '../intro/intro';
import { SurveyPage } from '../../survey_form/survey_form';
import { InstallationPage } from '../installation/installation';
import { WifiPage } from '../wifi/wifi';
import { ClosingPage } from '../closing/closing';
import { OverallPage } from '../overall/overall';
import { CfsPage } from '../cfs/cfs';
import { SuccessPage } from '../../success/success';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SurveyEntity } from '../../../data/entity/SurveyEntity';
import { RemarksEntity } from '../../../data/entity/RemarksEntity';
import { AdvanceCallEntity } from '../../../data/entity/AdvanceCallEntity';


/**
 * Generated class for the RemarksPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-remarks',
    templateUrl: 'remarks.html',
})
export class RemarksPage {

    public introPage = IntroPage;
    public SurveyPage = SurveyPage;
    public installationPage = InstallationPage;
    public wifiPage = WifiPage;
    public closingPage = ClosingPage;
    public overallPage = OverallPage;
    public cfsPage = CfsPage;
    public successPage = SuccessPage;

    loadProgress: number = 10;

    public title: String;
    public page: number;

    private advanceCallEntity: AdvanceCallEntity
    private surveryEntity: SurveyEntity
    private surveyForms: FormGroup

    constructor(public navCtrl: NavController, public navParams: NavParams,
        private formBuilder: FormBuilder, private alertCtrl: AlertController) {

        this.page = navParams.get("page");
        this.title = navParams.get("title");
        this.surveryEntity = navParams.get("surveryEntity");

        if(this.page == 1){
            this.advanceCallEntity = navParams.get("advanceCallEntity");
        }

        this.surveyForms = formBuilder.group({
            "whatWentWell": ["", Validators.required],
            "pointsForImprove": ["", Validators.required]
        });
    }

    ionViewDidLoad() {
        console.log("ionViewDidLoad ");
    }

    public onSubmit() {
        
        var remarksEntity = new RemarksEntity(this.surveyForms.value["whatWentWell"],
            this.surveyForms.value["pointsForImprove"]);

        switch (this.page) {
            case 1:

                // this.advanceCallEntity.setRemarks(remarksEntity);
                // this.surveryEntity.setAdvanceCall(this.advanceCallEntity);

                // console.log(this.advanceCallEntity);
                this.navCtrl.push(this.introPage, {
                    "surveyEntity" : this.surveryEntity
                });
                
                break;
            case 2:
                this.navCtrl.push(this.installationPage);
                break;
            case 3:
                this.navCtrl.push(this.wifiPage);
                break;
            case 4:
                this.navCtrl.push(this.closingPage);
                break;
            case 5:
                this.navCtrl.push(this.overallPage);
                break;
            case 6:
                this.navCtrl.push(this.cfsPage);
                break;
            default:
                this.navCtrl.push(this.successPage)
                break;
        }


    }

}
