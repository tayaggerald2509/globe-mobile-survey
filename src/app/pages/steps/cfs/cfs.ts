import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { RemarksPage } from '../remarks/remarks';

/**
 * Generated class for the CfsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-cfs',
    templateUrl: 'cfs.html',
})
export class CfsPage {

    remarksPage = RemarksPage;
    loadProgress: number = 10;

    isDisplayGreet: boolean = false;
    isDisplayPresentID: boolean = false;
    isDisplaySayName: boolean = false;

    constructor(public navCtrl: NavController, public navParams: NavParams) {

     }

    ionViewDidLoad() {
        console.log("ionViewDidLoad ");
    }

    public nextPage() {
        this.navCtrl.push(this.remarksPage, {
            page: 7,
            title: "CFS Technician Attributes"
        });
    }

    public previousPage() {

    }

}
