import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CfsPage } from './cfs';

@NgModule({
  declarations: [
    CfsPage,
  ],
  imports: [
    IonicPageModule.forChild(CfsPage),
  ],
})
export class CfsPageModule {

    
}
