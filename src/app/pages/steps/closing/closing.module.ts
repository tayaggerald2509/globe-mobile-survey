import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClosingPage } from './closing';

@NgModule({
  declarations: [
    ClosingPage,
  ],
  imports: [
    IonicPageModule.forChild(ClosingPage),
  ],
})
export class ClosingPageModule {

    
}
