import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { RemarksPage } from '../remarks/remarks';

/**
 * Generated class for the ClosingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-closing',
    templateUrl: 'closing.html',
})
export class ClosingPage {

    public remarksPage = RemarksPage;
    
    loadProgress: number = 80;

    public title: String;
    public page: number;

    constructor(public navCtrl: NavController, public navParams: NavParams) {
        
    }

    ionViewDidLoad() {
        console.log("ionViewDidLoad ");
    }

    public nextPage() {
        this.navCtrl.push(this.remarksPage, {
            page: 5,
            title: "Closing"
        });
    }

}
