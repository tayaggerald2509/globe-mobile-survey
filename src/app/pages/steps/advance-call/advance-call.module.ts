import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdvanceCallPage } from './advance-call';

@NgModule({
  declarations: [
    AdvanceCallPage,
  ],
  imports: [
    IonicPageModule.forChild(AdvanceCallPage),
  ],
})
export class AdvanceCallPageModule {

    
}
