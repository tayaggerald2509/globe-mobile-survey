import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { IntroPage } from '../intro/intro';
import { RemarksPage } from '../remarks/remarks';
import { SurveyEntity } from '../../../data/entity/SurveyEntity';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AdvanceCallEntity } from '../../../data/entity/AdvanceCallEntity';


/**
 * Generated class for the SplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-advance',
    templateUrl: 'advance-call.html',
})
export class AdvanceCallPage {

    public introPage = IntroPage;
    public remarksPage = RemarksPage;
    loadProgress: number = 10;

    public isDisplayDateOfVisit: boolean = false;
    public isDisplayTimeOfVisit: boolean = false;
    public isDisplayEtaCaused: boolean = false;
    public isDisplayAdvanceFee: boolean = false;
    public isDisplaySMSSent: boolean = false;

    private surveyEntity: SurveyEntity

    public surveyForms: FormGroup;

    constructor(public navCtrl: NavController, public navParams: NavParams,
        private formBuilder: FormBuilder, private alertCtrl: AlertController) { 
        this.surveyEntity = navParams.get("surveyEntity")
        this.surveyForms = formBuilder.group({
            "dateOfVisit" : ["", Validators.required],
            "timeOfVisit" : ["", Validators.required],
            "EtaCaused" : ["", Validators.required],
            "advanceFee" : ["", Validators.required],
            "SMSSent" : ["", Validators.required],
        });
    }

    ionViewDidLoad() {
        console.log("ionViewDidLoad ");
    }

    /**
     *  TODO: recode this
     */
    toggleDateOfVisit() {
        this.isDisplayDateOfVisit = !this.isDisplayDateOfVisit;
        this.isDisplayTimeOfVisit = false;
        this.isDisplayEtaCaused = false;
        this.isDisplayAdvanceFee = false;
        this.isDisplaySMSSent = false;
    }

    toggleTimeOfVisit() {
        this.isDisplayDateOfVisit = false;
        this.isDisplayTimeOfVisit = !this.isDisplayTimeOfVisit;
        this.isDisplayEtaCaused = false;
        this.isDisplayAdvanceFee = false;
        this.isDisplaySMSSent = false;
    }

    toggleEtaCaused() {
        this.isDisplayDateOfVisit = false;
        this.isDisplayTimeOfVisit = false;
        this.isDisplayEtaCaused = !this.isDisplayEtaCaused;
        this.isDisplayAdvanceFee = false;
        this.isDisplaySMSSent = false;
    }

    toggleAdvanceFee() {
        this.isDisplayDateOfVisit = false;
        this.isDisplayTimeOfVisit = false;
        this.isDisplayEtaCaused = false;
        this.isDisplayAdvanceFee = !this.isDisplayAdvanceFee;
        this.isDisplaySMSSent = false;
    }

    toggleSMSSent() {
        this.isDisplayDateOfVisit = false;
        this.isDisplayTimeOfVisit = false;
        this.isDisplayEtaCaused = false;
        this.isDisplayAdvanceFee = false;
        this.isDisplaySMSSent = !this.isDisplaySMSSent;
    }

    public onSubmit(){

        var advanceCallEntity = new AdvanceCallEntity(
            this.surveyForms.value["dateOfVisit"],
            this.surveyForms.value["timeOfVisit"],
            this.surveyForms.value["EtaCaused"],
            this.surveyForms.value["advanceFee"],
            this.surveyForms.value["SMSSent"],
            null
        );

        // this.surveyEntity.setAdvanceCall(advanceCallEntity);

        this.navCtrl.push(this.remarksPage, {
            page: 1,
            title: "Advance call",
            advanceCallEntity: advanceCallEntity,
            surveyEntity: this.surveyEntity
        });
    }
}
