import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { trigger, state, style, animate, transition } from '@angular/animations';

import { SurveyPage } from '../survey_form/survey_form';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';

/**
 * Generated class for the SplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
    animations: [
        trigger('visibilityChanged', [
            state('shown', style({ opacity: 1 })),
            state('hidden', style({ opacity: 0 })),
            transition('* => *', animate('500ms'))
        ])
    ]
})
export class LoginPage {

    public surveyPage = SurveyPage;

    public splash: boolean = true;
    public visibility: string = 'hidden';

    private formGroup: FormGroup;

    constructor(private androidFullScreen: AndroidFullScreen, public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder) {
        this.formGroup = formBuilder.group({
            name: ['', Validators.required]
        });

        this.androidFullScreen.isImmersiveModeSupported()
            .then(() => this.androidFullScreen.immersiveMode())
            .catch((error: any) => console.log(error));

    }

    ionViewDidLoad() {
        console.log("ionViewDidLoad ");
        setTimeout(() => {
            this.splash = false;
            console.log(this.splash);
        }, 4000);
    }

    onSubmit() {
        if (this.formGroup.valid) {
            this.navCtrl.push(this.surveyPage);
        }
    }
}
