import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { trigger, state, style, animate, transition } from '@angular/animations';

import { SurveyPage } from '../survey_form/survey_form';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginPage } from '../login/login';

/**
 * Generated class for the SplashPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-success',
    templateUrl: 'success.html',
    animations: [
        trigger('visibilityChanged', [
            state('shown', style({ opacity: 1 })),
            state('hidden', style({ opacity: 0 })),
            transition('* => *', animate('500ms'))
        ])
    ]
})
export class SuccessPage {

    public loginPage = LoginPage;
    public surveyPage = SurveyPage;

    public splash: boolean = true;
    public visibility: string = 'hidden';

    private formGroup: FormGroup;

    constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder) {
        this.formGroup = formBuilder.group({
            name: ['', Validators.required]
        });
     }

    ionViewDidLoad() {
        console.log("ionViewDidLoad ");
        setTimeout(() => {
            this.splash = false;
            console.log(this.splash);
        }, 4000);
    }

    onSubmit(){
        if(this.formGroup.valid){
            this.navCtrl.push(this.surveyPage);
        }
    }
    
    onDone(){
        this.navCtrl.push(this.surveyPage);
    }
}
