import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, AlertController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ProgressBarComponent } from '../app/components/progress-bar/progress-bar';


import { MyApp } from './app.component';
import { HomePage } from './pages/home/home';
import { LoginPage } from './pages/login/login';
import { SurveyPage } from './pages/survey_form/survey_form';
import { AdvanceCallPage } from './pages/steps/advance-call/advance-call';
import { IntroPage } from './pages/steps/intro/intro';
import { RemarksPage } from './pages/steps/remarks/remarks';
import { InstallationPage } from './pages/steps/installation/installation';
import { WifiPage } from './pages/steps/wifi/wifi';
import { ContentPage } from './pages/steps/content/content';
import { ClosingPage } from './pages/steps/closing/closing';
import { OverallPage } from './pages/steps/overall/overall';
import { CfsPage } from './pages/steps/cfs/cfs';
import { FormBuilder } from '@angular/forms';
import { SuccessPage } from './pages/success/success';

import { AndroidFullScreen } from '@ionic-native/android-full-screen';

@NgModule({
  declarations: [
    ProgressBarComponent,
    MyApp,
    LoginPage,
    SurveyPage,
    AdvanceCallPage,
    IntroPage,
    RemarksPage,
    InstallationPage,
    WifiPage,
    ContentPage,
    ClosingPage,
    OverallPage,
    CfsPage,
    SuccessPage,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    AdvanceCallPage,
    MyApp,
    LoginPage,
    SurveyPage,
    IntroPage,
    RemarksPage,
    InstallationPage,
    WifiPage,
    ContentPage,
    ClosingPage,
    OverallPage,
    CfsPage,
    SuccessPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BrowserAnimationsModule,
    FormBuilder,
    AlertController,
    AndroidFullScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
