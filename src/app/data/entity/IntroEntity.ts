import { RemarksEntity } from "./RemarksEntity";


export class IntroEntity {

    constructor(public arriveOnTime: String, public greetSubs: String, public presentId: String,
        public sayName: String, public statePurposeVisit: String, public planMonthlySee: String, public dataAllowance: String,
        public collectionFee: String,  public freebies: String,  public remarks: RemarksEntity) {
    }

    toJson(): any {
        return {
            "arriveOnTime": this.arriveOnTime,
            "introduceProperly":
            {
                "greetSubs": this.greetSubs,
                "presentId": this.presentId,
                "sayName": this.sayName
            },
            "statePurposeVisit": this.statePurposeVisit,
            "confirmServiceAvailed":
            {
                "planMonthlySee": this.planMonthlySee,
                "dataAllowance": this.dataAllowance,
                "collectionFee": this.collectionFee,
                "freebies": this.freebies
            },
            "remarks": this.remarks.toJson()
        }
    }
}