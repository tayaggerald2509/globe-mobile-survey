import { RemarksEntity } from "./RemarksEntity";


export class CFSTechAttribEntity {

    constructor(public complexity: String, public integrity: String, public malasakit: String,
        public serviceOriented: String, public eagerToLearn: String, public remarks: RemarksEntity) {
    }

    toJson(): any {
        return  {
            "complexity" : this.complexity,
            "integrity" : this.integrity,
            "malasakit" : this.malasakit,
            "serviceOriented" : this.serviceOriented,
            "eagerToLearn" : this.eagerToLearn,
            "remarks" : this.remarks.toJson()
        }
    }
}