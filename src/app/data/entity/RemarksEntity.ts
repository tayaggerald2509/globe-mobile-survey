

export class RemarksEntity {

    constructor(public whatWentWell: String, public pointsOfImprovement: String){}

    toJson() {
        return {
            "whatWentWell": this.whatWentWell,
            "pointsOfImprovement": this.pointsOfImprovement
        };
    }
}


