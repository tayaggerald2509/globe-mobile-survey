import { RemarksEntity } from "./RemarksEntity";


export class OverAllEntity {

    constructor(public collectCorrectFees: String, public helpInstallGlobeApp: String, public leaveBehindMaterial: String,
        public explainServiceCompletion: String, public askHelpBeforeLeaving: String, public remarksEntity: RemarksEntity) {
    }

    toJson(): any {
        return {
            "collectCorrectFees": this.collectCorrectFees,
            "helpInstallGlobeApp": this.helpInstallGlobeApp,
            "leaveBehindMaterial": this.leaveBehindMaterial,
            "explainServiceCompletion": this.explainServiceCompletion,
            "askHelpBeforeLeaving": this.askHelpBeforeLeaving,
            "whatWentWell": this.remarksEntity.whatWentWell,
            "pointsImprovement": this.remarksEntity.pointsOfImprovement
        }
    }
}