



export class CustomerDetailsEntity {

    constructor(public subsName: String, public serviceNo: String,
        public workOrderRef: String, public activity: String, public location: String,
        public repName: String, public relToSubs: String) {

    }

    toJson(): any {
        return {
            "subsName": this.subsName,
            "serviceNo": this.serviceNo,
            "workOrderRef": this.workOrderRef,
            "activity": this.activity,
            "location": this.location,
            "repName": this.repName,
            "relToSubs": this.relToSubs
        }
    }
}