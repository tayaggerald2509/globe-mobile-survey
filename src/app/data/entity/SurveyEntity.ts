import { CFSDetailsEntity } from '../entity/CFSDetailsEntity';
import { TechDetailsEntity } from './TechDetailsEntity';
import { CustomerDetailsEntity } from './CustomerDetailsEntity';
import { AdvanceCallEntity } from './AdvanceCallEntity';
import { IntroEntity } from './IntroEntity';
import { InstallationEntity } from './InstallationEntity';
import { WifiEntity } from './WifiEntity';
import { ClosingEntity } from './ClosingEntity';
import { OverAllEntity } from './OverAllEntity';
import { ContentEntity } from './ContentEntity';
import { CFSTechAttribEntity } from './CFSTechAttribEntity';

export class SurveyEntity {

    private surveyEntity: any;

    private cfsDetails: CFSDetailsEntity
    private techDetails: TechDetailsEntity
    private customerDetails: CustomerDetailsEntity

    public advanceCallEntity: AdvanceCallEntity
    public introEntity: IntroEntity
    public installationEntity: InstallationEntity

    private wifiEnitity: WifiEntity
    private contentEntity: ContentEntity
    private closingEntity: ClosingEntity
    private overAllEntity: OverAllEntity

    private cfsTechAttribute: CFSTechAttribEntity

    public setCFSDetails(cfsDetails: CFSDetailsEntity) {
        this.cfsDetails = cfsDetails;
    }

    setTechDetails(techDetails: TechDetailsEntity) {
        this.techDetails = techDetails;
    }

    setCustomerDetails(customerDetails: CustomerDetailsEntity) {
        this.customerDetails = customerDetails;
    }

    setAdvanceCall(advanceCallEntity: AdvanceCallEntity) {
        this.advanceCallEntity = advanceCallEntity;
    }

    setIntro(introEntity: IntroEntity) {
        this.introEntity = introEntity;
    }

    setInstallationEntity(installationEntity: InstallationEntity) {
        this.installationEntity = installationEntity;
    }

    setWifi(wifiEnitity: WifiEntity) {
        this.wifiEnitity = wifiEnitity;
    }

    setContent(contentEntity: ContentEntity){
        this.contentEntity = contentEntity;
    }

    setClosing(closingEntity: ClosingEntity) {
        this.closingEntity = closingEntity;
    }

    setOverAll(overAllEntity: OverAllEntity){
        this.overAllEntity = overAllEntity;
    }
    
    setCFSTechAttrib(cfsTechAttribute : CFSTechAttribEntity){
        this.cfsTechAttribute = cfsTechAttribute;
    }

    toJson() {
        return JSON.stringify(this.surveyEntity = {
            "surveys": [
                {
                    "cfsDetails": this.cfsDetails.toJson(),
                    "technician": this.techDetails.toJson(),
                    "customerDetails": this.customerDetails.toJson(),
                    "step1": this.advanceCallEntity.toJson(),
                    // "step2": this.introEntity.toJson(),
                    // "step3": this.installationEntity.toJson(),
                    // "step4": this.wifiEnitity.toJson(),
                    // "step5": this.contentEntity.toJson(),
                    // "step6": this.closingEntity.toJson(),
                    // "overall" : this.overAllEntity.toJson(),
                    // "cfsTechAttribute" : this.cfsTechAttribute.toJson()
                }
            ]
        })
    }
}