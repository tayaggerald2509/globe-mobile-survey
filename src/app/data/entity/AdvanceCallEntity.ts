import { RemarksEntity } from "./RemarksEntity";


export class AdvanceCallEntity {

    constructor(public dateOfVisit: String, public timeOfVisit: String, public newEta: String,
        public advFees: String, public smsSent: String, public remarks: RemarksEntity) {
    }

    toJson(): any {
        return {
            "advanceCall":
            {
                "dateOfVisit": this.dateOfVisit,
                "timeOfVisit": this.timeOfVisit,
                "newEta": this.newEta,
                "advFees": this.advFees,
                "smsSent": this.smsSent
            },
            "remarks": this.remarks.toJson()
        }
    }
}