

export class CFSDetailsEntity {
    
    constructor(public details: String, public observerName: String, public observerID: String,
        public dateOfObservation: String, public contractor: String, public region: String, public foAreaHead: String,
        public foTeamLeader: String){
    }

    toJson() : any {
        return {
            "details": this.details,
            "observerName" : this.observerName,
            "observerID" : this.observerID,
            "dateOfObservation" : this.dateOfObservation,
            "contractor" : this.contractor,
            "region" : this.region,
            "foAreaHead" : this.foAreaHead,
            "foTeamLeader" : this.foTeamLeader
        }
    }
}