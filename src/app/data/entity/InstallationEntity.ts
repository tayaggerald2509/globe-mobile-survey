import { RemarksEntity } from "./RemarksEntity";


export class InstallationEntity {

    constructor(public outdoorActivity: String, public indoorActivity: String, public wearHat: String,
        public wearSafetyBelt: String, public useSafetyCone: String, public puncherForJumpering: String,
        public laptopForSpeedTest: String, public others: String, public indoorWiringProperly: String,
        public completeStableConnection: String, public methodicalComplete: String, public tidyUpWork: String,
        public remarks: RemarksEntity) {
    }

    toJson(): any {
        return {
            "provideOverview": {
                "outdoorActivity": this.outdoorActivity,
                "indoorActivity": this.indoorActivity
            },
            "compliantSafety": {
                "wearHat": this.wearHat,
                "wearSafetyBelt": this.wearSafetyBelt,
                "useSafetyCone": this.useSafetyCone
            },
            "properTools": {
                "puncherForJumpering": this.puncherForJumpering,
                "laptopForSpeedTest": this.laptopForSpeedTest,
                "others": this.others
            },
            "indoorWiringProperly": this.indoorWiringProperly,
            "completeStableConnection": this.completeStableConnection,
            "methodicalComplete": this.methodicalComplete,
            "tidyUpWork": this.tidyUpWork,
            "whatWentWell": this.remarks.whatWentWell,
            "pointsImprovement": this.remarks.pointsOfImprovement
        }
    }
}