import { RemarksEntity } from "./RemarksEntity";


export class ContentEntity {

    constructor(public freebiesIncluded: String, public howToActivate: String, public setupDevice: String,
        public remarksEntity: RemarksEntity) {
    }

    toJson(): any {
        return {
            "educateSubs":
            {
                "freebiesIncluded": this.freebiesIncluded,
                "howToActivate": this.howToActivate,
                "setupDevice": this.setupDevice
            },
            "whatWentWell": this.remarksEntity.whatWentWell,
            "pointsImprovement": this.remarksEntity.pointsOfImprovement
        }
    }
}