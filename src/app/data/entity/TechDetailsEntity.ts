

export class TechDetailsEntity {

    constructor(public srTechName: String, public srTechIdNum: String, 
        public techName: String, public techIdNum: String) {

    }   

    toJson(): any {
        return {
            "srTechName": this.srTechName,
            "srTechIdNum": this.srTechIdNum,
            "techName": this.srTechName,
            "techIdNum": this.techIdNum
        }
    }
}