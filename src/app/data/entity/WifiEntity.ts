import { RemarksEntity } from "./RemarksEntity";


export class WifiEntity {

    constructor(public speedTestWifi101: String, public infromedInternetSpeed: String, public placementRouter: String,
        public factorsAffecting: String, public offerAirties: String, public remarksEntity: RemarksEntity) {
    }

    toJson(): any {
        return {
            "speedTestWifi101": this.speedTestWifi101,
            "infromedInternetSpeed": this.infromedInternetSpeed,
            "explainBasicWifi": 
            {
                "placementRouter": this.placementRouter,
                "factorsAffecting": this.factorsAffecting
            },
            "offerAirties": this.offerAirties,
            "whatWentWell": this.remarksEntity.whatWentWell,
            "pointsImprovement": this.remarksEntity.whatWentWell
        }
    }    
}