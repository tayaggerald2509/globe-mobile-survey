
import { HTTP } from '@ionic-native/http';

export class Api {

    private BASEURL = "https://globe-survey.herokuapp.com"

    constructor(public http: HTTP) { }

    post(apiUrl: string, data: any = {}) {
        return this.http.post(apiUrl, JSON.stringify(data), {})
            .then(data => {
                console.log(data);
            }).catch(error => {
                console.log(error);
            });
    }

}